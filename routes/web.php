<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HotelController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\RoomController;
use App\Http\Controllers\BookingController;
use App\Http\Controllers\EmployeeController;
use App\Http\Controllers\HistoryController;
use App\Http\Controllers\CustomerController;
use App\Http\Controllers\ServiceController;
use App\Http\Controllers\TypeRoomController;
use App\Http\Controllers\MessageController;
use App\Http\Controllers\Auth\{LoginController, RegisterController, ResetPasswordController};
use App\Http\Middleware\{User, Admin};
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
# Login & Register & Forgot password
Route::post('/send_login', [LoginController::class, 'sendLogin'])->name('sendLogin');
Route::post('/send_register', [RegisterController::class, 'sendRegister'])->name('sendRegister');
Route::post('/send_email_password', [ResetPasswordController::class, 'sendEmailPassword'])->name('sendEmailPassword');
Route::get('/logout', [LoginController::class, 'doLogout'])->name('doLogout');
Route::get('/logoutadmin',[LoginController::class,'doLogoutAdmin'])->name('doLogoutAdmin');
# End Login & Register & Forgot password
# Customer
Route::get('/user_profile', [CustomerController::class, 'index'])->name('profile')->middleware(User::class);
Route::get('/user_profile_edit', [CustomerController::class, 'showPageEdit'])->name('showPageEdit')->middleware(User::class);
Route::post('/send_user_edit', [CustomerController::class, 'doUserEdit'])->name('doUserEdit')->middleware(User::class);
Route::post('/changeavatar',[CustomerController::class,'changeAvatar'])->middleware(User::class);
Route::get('/user_password_change', [CustomerController::class, 'showPasswordChange'])->name('showPasswordChange')->middleware(User::class);
Route::post('/send_password_change', [CustomerController::class, 'doChangePassword'])->name('doChangePassword')->middleware(User::class);
Route::get('/history',[CustomerController::class, 'showHistory'])->name('showHistory')->middleware(User::class);
# End customer

Route::get('/', [HotelController::class,'home'])->name('home');
Route::get('/room', [HotelController::class,'room'])->name('room');
Route::get('/about', [HotelController::class,'aboutus'])->name('aboutus');
Route::get('/cart',[CustomerController::class,'showCart'])->name('cart');
Route::get('/detailroom={id}',[HotelController::class,'detailroom'])->name('detailroom');
Route::get('/admin/employee',[AdminController::class,'employee'])->name('employee')->middleware(Admin::class);
Route::post('/postRate/{id}', [HotelController::class, 'postRate'])->name('postrate')->middleware(User::class);
Route::get('/admin',[AdminController::class,'index'])->name('adminindex')->middleware(Admin::class);
Route::get('/admin/profile',[AdminController::class,'profile'])->name('adminprofile')->middleware(Admin::class);
Route::put('/admin/profile',[AdminController::class,'editprofile'])->name('updateprofile')->middleware(Admin::class);
Route::put('/admin/profile/newpass',[AdminController::class,'doChangePassword'])->name('changepassadmin')->middleware(Admin::class);
Route::put('/admin/profile/newavatar',[AdminController::class,'changeAvatar'])->name('changeavatar')->middleware(Admin::class);
Route::resource('rooms', RoomController::class)->middleware(Admin::class);
Route::resource('types', TypeRoomController::class)->middleware(Admin::class);
Route::resource('bookings', BookingController::class)->middleware(Admin::class);
Route::resource('employees', EmployeeController::class)->middleware(Admin::class);
Route::resource('services', ServiceController::class)->middleware(Admin::class);
Route::resource('message', MessageController::class)->middleware(Admin::class);
Route::get('/contact', [MessageController::class,'create'])->name('contact');
Route::get('/profile',[CustomerController::class,'index'])->name('profile')->middleware(User::class);
Route::get('/bookings/bill/{id}',[BookingController::class,'printbill'])->name('bill')->middleware(Admin::class);
Route::delete('/cart/{id}',[CustomerController::class,'destroy'])->name('deletecart')->middleware(User::class);
#Resetpass with mail
Route::get('/resetpassword=token={token}',[CustomerController::class,'getChangePass']);
Route::post('/change-password-with-token',[CustomerController::class,'postChangePass']);


// Route::get('/admin/room',[RoomController::class,'index'])->name('adminrooms');
// Route::get('/admin/user',[UserController::class,'index'])->name('adminusers');
// Route::get('/admin/booking',[BookingController::class,'index'])->name('adminbookings');
// Route::get('/admin/employee',[EmployeeController::class,'index'])->name('adminemployees');
// Route::get('/admin/services',[ServiceController::class,'index'])->name('adminservices');
// Route::get('/admin/user/create',[UserController::class,'create'])->name('usercreate');
Route::resource('users', UserController::class);
Route::get('/reset',function(){
    return view('emails.mailreset');
});
Route::get('/admin/bill', function(){
    return view('admin.bookings.bill');
});
Route::get('/admin/login', function() {
    return view('admin.login');
});
Route::get('admin/editbooking',function(){
return view('admin.bookings.editbooking');
});
Route::post('/send-login-admin', [LoginController::class, 'doLoginAdmin']);
// Route::get('/admin/rooms/edit', function() {
//     return view('admin.rooms.editroom');
// });
Route::post('/room/{id}',[CustomerController::class,'pushToCart'])->name('pushToCart')->middleware(User::class);
Route::put('/sendBookingToAdmin/{id}',[CustomerController::class,'sendBookingToAdmin'])->name('sendBookingToAdmin')->middleware(User::class);
Route::get('/cart/{id}',[CustomerController::class,'cartAjax'])->middleware(User::class);




//Multiple langue
Route::get('locale/{locale}', function ($locale){
    Session::put('locale', $locale);
    return redirect()->back();
});
