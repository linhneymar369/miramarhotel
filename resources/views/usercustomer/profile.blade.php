@extends('base')
@section('content')
<br>
<div class="container" style="min-height: 1000px;">
    <div class="row my-2">
        <div class="col-lg-8 order-lg-2">
            <ul class="nav nav-tabs">
                <li class="nav-item">
                    <a href="" data-target="#profile" data-toggle="tab" class="nav-link active">{{__('sentence.profile')}}</a>
                </li>
                <li class="nav-item">
                    <a href="" data-target="#edit_info" data-toggle="tab" class="nav-link">{{__('sentence.Edit profile')}}</a>
                </li>
                <li class="nav-item">
                    <a href="" data-target="#change_password" data-toggle="tab" class="nav-link">{{__('sentence.Change Password')}}</a>
                </li>
            </ul>
            <div class="tab-content py-4">
                <div class="tab-pane active" id="profile">
                    <h5 class="mb-3">{{__('sentence.User Profile')}}</h5>
                    <div class="row">
                        <div class="col-md-6">
                            <h6>{{__('sentence.Name')}}</h6>
                            <p style="font-size: 18px">
                                {{$data->name}}
                            </p><hr>
                            <h6>{{__('sentence.email')}}</h6>
                            <p style="font-size: 18px">
                                {{$data->email}}
                            </p><hr>
                        </div>
                        <div class="col-md-6">
                            <h6>{{__('sentence.Address')}}</h6>
                            @if ($data->address == null)
                            <p style="font-size: 18px">{{__('sentence.Null')}}</p>
                            @else
                            <p style="font-size: 18px">{{$data->address}}</p>
                            @endif
                            <hr>
                            <h6>{{__('sentence.Phone')}}</h6>
                            @if ($data->phone == null)
                            <p style="font-size: 18px">{{__('sentence.Null')}}</p>
                            @else
                            <p style="font-size: 18px">{{$data->phone}}</p>
                            @endif
                            <hr>
                        </div>
                    </div>
                    <!--/row-->
                </div> <div class="tab-pane" id="edit_info">
                    <form action="/send_user_edit" method="POST" role="form">
                        @csrf
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label form-control-label">{{__('sentence.Name')}}</label>
                            <div class="col-lg-9">
                                <input class="form-control" name="name" type="text" value="{{$data->name}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label form-control-label">{{__('sentence.Gender')}}</label>
                            <div class="col-lg-9">
                                <select name="gender" id="" class="form-control">
                                @if($data->gender == 1)
                                <option value="1">Man</option>
                                <option value="2">Lady</option>
                                @else 
                                <option value="2">Lady</option>
                                <option value="1">Man</option>
                                @endif
                            </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label form-control-label">{{__('sentence.Birth day')}}</label>
                            <div class="col-lg-9">
                                <input type="date" name="birth_day" class="form-control" type="text" value="{{$data->birth_day}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label form-control-label">{{__('sentence.Address')}}</label>
                            <div class="col-lg-9">
                                <input class="form-control" name="address" value="{{$data->address}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label form-control-label">{{__('sentence.Identify Card')}}</label>
                            <div class="col-lg-9">
                                <input class="form-control" name="identify_card" value="{{$data->identify_card}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label form-control-label">{{__('sentence.Phone')}}</label>
                            <div class="col-lg-9">
                                <input class="form-control" name="phone" type="text" value="{{$data->phone}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label form-control-label"></label>
                            <div class="col-lg-9">
                                <button type="submut" class="btn btn-primary">{{__('sentence.Save Changes')}}</button>
                            </div>
                        </div>
                    </form>
                </div><div class="tab-pane" id="change_password">
                    <form action="/send_password_change" method="POST" role="form">
                            @csrf
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label form-control-label" style="font-weight: bold;font-family: sans-serif;">{{__('sentence.Old Password')}}</label>
                                <div class="col-lg-9">
                                    <input class="form-control" name="oldPassword" type="text" value="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label form-control-label" style="font-weight: bold;font-family: sans-serif;">{{__('sentence.New Password')}}</label>
                                <div class="col-lg-9">
                                    <input class="form-control" name="newPassword" type="password" value="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label form-control-label" style="font-weight: bold;font-family: sans-serif;">{{__('sentence.Confirm New Password')}}</label>
                                <div class="col-lg-9">
                                    <input class="form-control" name="confirmPassword" type="password" value="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label form-control-label"></label>
                                <div class="col-lg-9">
                                    
                                    <button type="submit" class="btn btn-primary">{{__('sentence.Save Changes')}}</button>
                                </div>
                            </div>
                        </form>
                    </div>
            </div>
        </div>
        <div class="col-lg-4 order-lg-1 text-center">
            <form action="/changeavatar" method="POST" enctype="multipart/form-data" id="changeAvatar">
                @csrf
            <img src=" @if($data->avatar !== null) {{asset('images/avatar/'.$data->avatar)}}
        @else
        {{asset('images/user.png')}}
        @endif" class="mx-auto img-fluid img-circle d-block rounded-circle" id="output" alt="avatar" style="border-radius: 50%; width: 10rem; height: 10rem;">
            <h6 class="mt-2">{{__('sentence.Upload your avatar')}}</h6>
            <label class="custom-file">
                <input type="file" name="avatar" id="file-upload" accept="image/*" onchange="loadFile(event)" class="custom-file-input">
                <span id="file-name" class="custom-file-control">{{__('sentence.Choose file')}}</span>
            </label>
            <br>
            <hr>
            <div class="form-group row">
                <label class="col-lg-3 col-form-label form-control-label"></label>
                <div class="col-lg-6">
                    <input type="submit" class="btn btn-primary" value="{{__('sentence.Save Avatar')}}">
                </div>
            </div>
        </form>
        </div>
    </div>
</div>
<script>
    var loadFile = function (event) {
    var output = document.getElementById("output");
    output.src = URL.createObjectURL(event.target.files[0]);
    output.onload = function () {
        URL.revokeObjectURL(output.src);
    };
};
</script>
@endsection