@extends('base')
@section('content')
<div class="container">
    <form action="/send_password_change" method="post">
        @csrf
        <div class="form-group">
            <label for="">Old password:</label>
            <input type="password" name="oldPassword" id="" class="form-control">
            <label for="">New password:</label>
            <input type="password" name="newPassword" id="" class="form-control">
            <label for="">Confirm password:</label>
            <input type="password" name="confirmPassword" id="" class="form-control">
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>
@endsection