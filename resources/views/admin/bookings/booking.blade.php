@extends('adminLayout')
@section('content')

<div class="right_col" style="min-height: 953px;">
    <h1 style="text-align:center;">Bookings</h1>
    <form action="" method="GET" class="frmsearch" id="searchform">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text widthcol2" id="basic-addon1" style="width: 95px;">User:</span>
                        </div>
                        <input type="text" id="name" class="form-control" name="username" placeholder="Enter name user"
                            aria-label="Username" aria-describedby="basic-addon1" value="{{request()->get('username')}}"
                            maxlength="115">
                    </div>
                </div>
                <div class="col">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text widthcol2" id="basic-addon1" style="width: 95px;">Type
                                room:</span>
                        </div>
                            <select name="typeroom" id="" value="{{request()->get('typeroom')}}" class="form-control">
                                <option value="0">All</option>
                                <option value="1">Standard Room</option>
                                <option value="2">Intermediate Room</option>
                                <option value="3">Luxury Room</option>
                                <option value="4">VIP Room</option>
                                <option value="5">Presidential Room</option>
                            </select>
                    </div>
                </div>
            </div>
            <div class=row>
                <div class="col">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text widthcol2" id="basic-addon1" style="width: 95px;">ID
                                room:</span>
                        </div>
                        <input type="text" id="id" class="form-control" name="idroom" placeholder="Enter id room"
                            aria-label="Username" aria-describedby="basic-addon1" value="{{request()->get('idroom')}}">
                    </div>
                </div>
                <div class="col">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text widthcol2" id="basic-addon1"
                                style="width: 95px;">Status:</span>
                        </div>
                            <select id="" class="form-control" name="status" value="{{request()->get('status')}}">
                                <option value="">All</option>
                                <option value="1">In Booking Cart</option>
                                <option value="2">Need Confirm</option>
                                <option value="3">Confirmation</option>
                                <option value="4">Paid</option>
                            </select>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row" style="margin: 20px">
                    <div class="col-md-4"></div>
                    <div class=" col-12 col-ms-8 col-md-8 col-xs-6">
                        <button type="submit" class="btn btn-primary col search" style="width:25%;">Search</button>
                    <a href="{{route('bookings.create')}}" class="">
                            <button type="button" class="btn btn-info col" style="width:25%;">Create</button>
                        </a>
                    </div>
                </div>
            </div>
    </form>
    <div class="row mt-2">
        <div class="col-md-12 col-sm-12">
            <div class="x_panel">
                <div class="x_content">
    <table class="table table-striped">
        <thead>
            <tr>
                <th scope="col">STT</th>
                <th scope="col">ID room</th>
                <th scope="col">Type room</th>
                <th scope="col">User</th>
                <th scope="col">Check in</th>
                <th scope="col">Check out</th>
                <th scope="col">Service</th>
                <th scope="col">Price</th>
                <th scope="col">Total</th>
                <th scope="col">Status</th>
                <th scope="col">Action</th>
            </tr>
        </thead>
        <tbody>
            <?php $count = 1; ?>
            @foreach($bookings as $booking)
            <tr>
                <th scope="row">{{$bookings->perPage()*($bookings->currentPage()-1)+$count}}</th>
                <td>{{$booking->room_id}}</td>
                <td>{{$booking->rooms->types->name}}</td>
                <td>{{$booking->users->name}}</td>
                <td>{{Carbon\Carbon::parse($booking->check_in)->format('d-m-Y')}}</td>
                <td>{{Carbon\Carbon::parse($booking->check_out)->format('d-m-Y')}}</td>
                <td>{{$booking->services->price}}$</td>
                <td>{{$booking->rooms->types->price*$booking->rooms->amount_user}}$</td>
                <td>{{$booking->total}}$</td>
                @if ($booking->status == 1)
                <td>In Booking Cart</td>
                @elseif($booking->status == 2)
                <td>Need Confirm</td>
                @elseif($booking->status == 3)
                <td>Confirmation</td>
                @elseif($booking->status == 4)
                <td>Paid</td>
                @endif
                <td style="display: flex;">
                    <a href="{{route('bookings.edit', $booking->id)}}" style="color:blue;">
                        <svg width="1.5em" height="1.5em" viewBox="0 0 16 16" class="bi bi-pencil-square"
                            fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path
                                d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456l-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z" />
                            <path fill-rule="evenodd"
                                d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z" />
                        </svg>
                    </a>
                    <a href="" data-toggle="modal" data-target="#exampleModal"
                        onClick="onClickDelete({{$booking->id}})" style="color:red;">
                        <svg width="1.5em" height="1.5em" viewBox="0 0 16 16" class="bi bi-trash" fill="currentColor"
                            xmlns="http://www.w3.org/2000/svg">
                            <path
                                d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z" />
                            <path fill-rule="evenodd"
                                d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4L4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z" />
                        </svg>
                    </a>
                <a href="{{route('bill',$booking->id)}}" style="color:green;">
                        <svg width="1.5em" height="1.5em" viewBox="0 0 16 16" class="bi bi-printer" fill="currentColor"
                            xmlns="http://www.w3.org/2000/svg">
                            <path
                                d="M11 2H5a1 1 0 0 0-1 1v2H3V3a2 2 0 0 1 2-2h6a2 2 0 0 1 2 2v2h-1V3a1 1 0 0 0-1-1zm3 4H2a1 1 0 0 0-1 1v3a1 1 0 0 0 1 1h1v1H2a2 2 0 0 1-2-2V7a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v3a2 2 0 0 1-2 2h-1v-1h1a1 1 0 0 0 1-1V7a1 1 0 0 0-1-1z" />
                            <path fill-rule="evenodd"
                                d="M11 9H5a1 1 0 0 0-1 1v3a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1v-3a1 1 0 0 0-1-1zM5 8a2 2 0 0 0-2 2v3a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2v-3a2 2 0 0 0-2-2H5z" />
                            <path d="M3 7.5a.5.5 0 1 1-1 0 .5.5 0 0 1 1 0z" />
                        </svg>
                    </a>
                </td>
            </tr>
            <?php $count++; ?>
            @endforeach
        </tbody>
    </table>
    <form style="display:inline" action="" method="post" id="delete-form">
        @csrf
        @method('DELETE')
        <div class="modal" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content" style="border-radius: 1rem;">
                    <div class="modal-header" style="background-color: #2A3F54;">
                        <h5 class="modal-title" id="exampleModalLabel" style="color:white;">DELETE</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <h1 style="text-align:center;">Are you sure?</h1>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal"
                            style="width: 100px;">Close</button>
                        <button type="submit" class="btn btn-danger" data-toggle="modal" data-target="#successdelete"
                            onClick="submitdelete()" style="width: 67px;">Yes</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <div class="d-flex justify-content-end nav">
        {!! $bookings->withQueryString()->links('pagination.paginate') !!}
    </div>
    </div>
    </div>
        </div>
    </div>
</div>
@if (Session::has('message'))
<div id="snackbar">{{ Session::get('message') }}</div>
<script>
    var x = document.getElementById("snackbar");
    x.className = "show";
    setTimeout(function() {
        x.className = x.className.replace("show", "");
    }, 3000);

</script>
</div>
@endif
<script>
    const onClickDelete = (id) => {
        const deleteForm = document.getElementById('delete-form');
        deleteForm.action = '/bookings/' + id;
    };
    
    function submitdelete() {
        deleteForm.submit();
    }
    </script>
@endsection