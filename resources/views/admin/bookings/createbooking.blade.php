@extends('adminLayout')
@section('content')
<div class="right_col" style="min-height: 953px;">
    <div class="row mt-2">
        <div class="col-md-12 col-sm-12">
            <div class="x_panel">
                <div class="x_content">
    <h1 style="text-align: center;">Create Booking</h1>
<form action="{{route('bookings.store')}}" method="POST" id="form-create">
        @csrf  
        <div class="form-group">
        <label for="">Room id:</label>
        <input type="number" name="room" id="" class="form-control">
        </div>
        <div class="form-group">
        <label for="">Service:</label>
        <select name="service" id="" class="form-control">
            @foreach ($services as $service)
            <option value="{{$service->id}}">{{$service->name}}</option>
            @endforeach
        </select>
        </div>
        <div class="form-group">
        <label for="">User:</label>
        <select name="user" id="" class="form-control">
            @foreach ($users as $user)
            <option value="{{$user->id}}">{{$user->name}}</option>
            @endforeach
        </select>
        </div>
        <div class="form-group">
        <label for="">Check-in:</label>
        <input type="date" name="checkin" id="" class="form-control">
        </div>
        <div class="form-group">
        <label for="">Check-out:</label>
        <input type="date" name="checkout" id="" class="form-control">
        </div>
        <div class="form-group">
        <label for="">Total:</label>
        <input type="number" name="total" id="" class="form-control">
        </div>
        <div class="form-group">
        <label for="">Status:</label>
        <select name="status" id="" class="form-control">
            <option value="3">Confirmation</option>
            <option value="4">Paid</option>
        </select>
        </div>
        <button class="btn btn-secondary" style="margin-left: 45%;margin-top: 20px;">Submit</button>
    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@if ($errors->any())
            <div id="snackbar">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
                <script>
                    var x = document.getElementById("snackbar");
                    x.className = "show";
                    setTimeout(function() {
                        x.className = x.className.replace("show", "");
                    }, 5000);
                </script>
            <br />
        @endif
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
    <script>
    $("#form-create").validate({
    rules: {
        room: {
            required: true,
            maxlenght: 2000,
            number: true,
        },
        user: {
            required: true,
        },
        service: {
            required: true,
        },
        checkin: {
            required: true,
            date: true,
        },
        checkout: {
            required: true,
            date: true,
        },
        total: {
            required: true,
            maxlenght: 2000,
            number: true,
        },
        status: {
            required: true,
        }
    },
    errorElement: 'span',
    errorPlacement: function(error, element) {
        error.addClass('invalid-feedback');
        element.closest('.form-group').append(error);
    },
    highlight: function(element, errorClass, validClass) {
        $(element).addClass('is-invalid');
    },
    unhighlight: function(element, errorClass, validClass) {
        $(element).removeClass('is-invalid');
    }
});
</script>
@endsection