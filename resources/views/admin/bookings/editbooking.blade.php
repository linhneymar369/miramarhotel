@extends('adminLayout')
@section('content')
<div class="right_col" style="min-height: 953px;">
    <div class="row mt-2">
        <div class="col-md-12 col-sm-12">
            <div class="x_panel">
                <div class="x_content">
    <h1 style="text-align: center;">Edit Booking</h1>
<form action="{{route('bookings.update',$booking)}}" method="post">
    @method('put')
        @csrf
        <label for="">Room id:</label>
        <input type="number" name="room" id="" class="form-control" value="{{$booking->room_id}}">
        <label for="">Service:</label>
        <select name="service" id="" class="form-control">
            @foreach ($services as $service)
            <option value="{{$service->id}}">{{$service->name}}</option>
            @endforeach
        </select>
        <label for="">User:</label>
    <select name="user" id="" class="form-control" >
    <option value="{{$booking->user_id}}">{{$booking->users->name}}</option>
            @foreach ($users as $user)
            <option value="{{$user->id}}">{{$user->name}}</option>
            @endforeach
        </select>
        <label for="">Check-in:</label>
    <input type="date" name="checkin" id="" class="form-control" value="{{Carbon\Carbon::parse($booking->check_in)->format('Y-m-d')}}">
        <label for="">Check-out:</label>
    <input type="date" name="checkout" id="" class="form-control" value="{{Carbon\Carbon::parse($booking->check_out)->format('Y-m-d')}}">
        <label for="">Total:</label>
    <input type="number" name="total" id="" class="form-control" value="{{$booking->total}}">
        <label for="">Status:</label>
        <select name="status" id="" class="form-control">
            @if($booking->status == 1)
            <option value="1">In Booking Cart</option>
            <option value="2">Need Confirm</option>
            <option value="3">Confirmation</option>
            <option value="4">Paid</option>
            @elseif($booking->status == 2)
            <option value="2">Need Confirm</option>
            <option value="3">Confirmation</option>
            <option value="4">Paid</option>
            @elseif($booking->status == 3)
            <option value="3">Confirmation</option>
            <option value="2">Need Confirm</option>
            <option value="4">Paid</option>
            @elseif($booking->status == 4)
            <option value="4">Paid</option>
            <option value="3">Confirmation</option>
            @endif
        </select>
        <button class="btn btn-secondary" style="margin-left: 45%;margin-top: 20px;">Submit</button>
    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@if ($errors->any())
            <div id="snackbar">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
                <script>
                    var x = document.getElementById("snackbar");
                    x.className = "show";
                    setTimeout(function() {
                        x.className = x.className.replace("show", "");
                    }, 3000);
                </script>
            <br />
        @endif
@endsection