@extends('adminLayout')
@section('content')
<div class="right_col" style="min-height: 953px;">
    <div class="row mt-2">
        <div class="col-md-12 col-sm-12">
            <div class="x_panel">
                <div class="x_content">
<h1 style="text-align: center;">Edit Type Room</h1>
<form action="{{route('types.update',$type)}}" method="post">
@method('put')
@csrf       
    <div class="form-group">
        <label for="">Name:</label>
        <input type="text" name="name" id="" class="form-control" value="{{$type->name}}">
        <label for="">Price:</label>
        <input type="number" name="price" id="" class="form-control" value="{{$type->price}}">
        <button class="btn btn-primary" style="margin-left: 45%;margin-top: 20px;">Submit</button>
    </div>
    </form>
</div>
            </div>
        </div>
    </div>
</div>
@if ($errors->any())
            <div id="snackbar">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
                <script>
                    var x = document.getElementById("snackbar");
                    x.className = "show";
                    setTimeout(function() {
                        x.className = x.className.replace("show", "");
                    }, 3000);
                </script>
            <br />
        @endif
@endsection