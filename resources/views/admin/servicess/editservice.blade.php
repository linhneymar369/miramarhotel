@extends('adminLayout')
@section('content')
<div class="right_col" style="min-height: 953px;">
<h1 style="text-align: center;">Edit Service</h1>
<form action="{{route('services.update',$service)}}" method="post">
@method('put')
@csrf       
    <div class="form-group">
        <label for="">Name:</label>
        <input type="text" name="name" id="" class="form-control" value="{{$service->name}}">
        <label for="">Price:</label>
        <input type="number" name="price" id="" class="form-control" value="{{$service->price}}">
        <button class="btn btn-primary" style="margin-left: 45%;margin-top: 20px;">Submit</button>
    </div>
    </form>
</div>
@if ($errors->any())
            <div id="snackbar">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
                <script>
                    var x = document.getElementById("snackbar");
                    x.className = "show";
                    setTimeout(function() {
                        x.className = x.className.replace("show", "");
                    }, 3000);
                </script>
            <br />
        @endif
@endsection