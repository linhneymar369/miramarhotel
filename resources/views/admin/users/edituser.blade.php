@extends('adminLayout')
@section('content')
<div class="right_col" style="min-height: 953px;">
    <div class="row mt-2">
        <div class="col-md-12 col-sm-12">
            <div class="x_panel">
                <div class="x_content">
<h1 style="text-align: center;">Edit User</h1>
<form action="{{route('users.update',$user)}}" method="post">
@method('put') 
@csrf
<div class="form-group">
        <label for="">Email:</label>
        <input type="text" name="email" id="" class="form-control" value="{{$user->email}}">
        <label for="">Name:</label>
        <input type="text" name="name" id="" class="form-control" value="{{$user->name}}">
        <label for="">Status:</label>
        <select class="form-control" name="status" id="">
            @if($user->status == 0)
            <option value="0">Lock</option>
            <option value="1">Active</option>
            @else
            <option value="1">Active</option>
            <option value="0">Lock</option>
            @endif
        </select>
        <label for="">Role: </label>
        <Select class="form-control" name="role_id">
            <option value="1">Admin</option>
            <option value="2">Employee</option>
            <option value="3">Customer</option>
        </Select>
        <button class="btn btn-primary" style="margin-left: 45%;margin-top: 20px;">Submit</button>
    </div>
</form>
                </div>
            </div>
        </div>
    </div>
</div>
@if ($errors->any())
            <div id="snackbar">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
                <script>
                    var x = document.getElementById("snackbar");
                    x.className = "show";
                    setTimeout(function() {
                        x.className = x.className.replace("show", "");
                    }, 3000);
                </script>
            <br />
        @endif
@endsection