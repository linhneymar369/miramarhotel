@extends('adminLayout')
@section('content')
<div class="right_col" style="min-height: 953px;">
    <div class="row mt-2">
        <div class="col-md-12 col-sm-12">
            <div class="x_panel">
                <div class="x_content">
                    <h1 style="text-align: center;">Create User</h1>
                    <form action="{{route('users.store')}}" method="post" id="form-create">
                        @csrf       
                    <div class="form-group">
                    <label for="">Email:</label>
                    <input type="text" name="email" id="" class="form-control">
                    </div>
                    <div class="form-group">
                    <label for="">Password:</label>
                    <input type="text" name="password" id="" class="form-control">
                    </div>
                    <div class="form-group">
                    <label for="">Name:</label>
                    <input type="text" name="name" id="" class="form-control">
                    </div>
                    <div class="form-group">
                    <label for="">Gender:</label>
                    <select class="form-control" name="gender" id="">
                        <option value="1">Man</option>
                        <option value="2">Lady</option>
                    </select>
                    </div>
                    <div class="form-group">
                    <label for="">Birth day:</label>
                    <input type="date" name="birth_day" id="" class="form-control">
                    </div>
                    <div class="form-group">
                    <label for="">Address:</label>
                    <input type="text" name="address" id="" class="form-control">
                    </div>
                    <div class="form-group">
                    <label for="">Identify Card:</label>
                    <input type="text" name="identify_card" id="" class="form-control">
                    </div>
                    <div class="form-group">
                    <label for="">Phone:</label>
                    <input type="text" name="phone" id="" class="form-control">
                    </div>
                    <div class="form-group">
                    <label for="">Status:</label>
                    <select class="form-control" name="status" id="">
                        <option value="0">Lock</option>
                        <option value="1">Active</option>
                    </select>
                    </div>
                    <div class="form-group">
                    <label for="">Role: </label>
                    <Select class="form-control" name="role_id">
                        <option value="1">Admin</option>
                        <option value="2">Employee</option>
                        <option value="3">Customer</option>
                    </Select>
                    </div>
                    <button class="btn btn-primary" style="margin-left: 45%;margin-top: 20px;">Submit</button>
                
                </form>
                </div>
            </div>
        </div>
    </div>
</div>
@if ($errors->any())
            <div id="snackbar">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
                <script>
                    var x = document.getElementById("snackbar");
                    x.className = "show";
                    setTimeout(function() {
                        x.className = x.className.replace("show", "");
                    }, 5000);
                </script>
            <br />
        @endif
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
    <script>
    $("#form-create").validate({
    rules: {
        email: {
            required: true,
            maxlenght: 20,
            email: true,
        },
        password: {
            required: true,
        },
        name: {
            required: true,
        },
        gender: {
            required: true,
        },
        birth_day: {
            required: true,
            date: true,
        },
        address: {
            required: true,
            maxlenght: 2000,
        },
        identify_card: {
            required: true,
            number: true,
        },
        phone: {
            required: true,
            number: true,
        },
        status: {
            required: true,
        }
        ,
        role_id: {
            required: true,
        }
    },
    errorElement: 'span',
    errorPlacement: function(error, element) {
        error.addClass('invalid-feedback');
        element.closest('.form-group').append(error);
    },
    highlight: function(element, errorClass, validClass) {
        $(element).addClass('is-invalid');
    },
    unhighlight: function(element, errorClass, validClass) {
        $(element).removeClass('is-invalid');
    }
});
</script>
@endsection