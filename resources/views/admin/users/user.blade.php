@extends('adminLayout')
@section('content')
<div class="right_col" style="min-height: 953px;">
    <h1 style="text-align:center;">Users</h1>
    <form action="" method="GET" class="frmsearch" id="searchform">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text widthcol2" id="basic-addon1" style="width: 95px;">ID:</span>
                        </div>
                        <input type="text" id="id" class="form-control" name="id" placeholder="Enter id user"
                            maxlength="10" aria-label="Username" aria-describedby="basic-addon1"
                            value="{{request()->get('id')}}">
                    </div>
                </div>
                <div class="col">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text widthcol2" id="basic-addon1" style="width: 95px;">Name:</span>
                        </div>
                        <input type="text" id="name" class="form-control" name="name" placeholder="Enter name user"
                            aria-label="Username" aria-describedby="basic-addon1" value="{{request()->get('name')}}"
                            maxlength="115">
                    </div>
                </div>
                <div class="col">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text widthcol2" id="basic-addon1" style="width: 95px;">Role:</span>
                        </div>
                        <input type="text" id="role" class="form-control" name="role" placeholder="Enter role"
                            aria-label="Username" aria-describedby="basic-addon1" value="{{request()->get('role')}}"
                            maxlength="115">
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row" style="margin: 20px">
                <div class="col-md-4"></div>
                <div class=" col-12 col-ms-8 col-md-8 col-xs-6">
                    <button type="submit" class="btn btn-primary col search" style="width:25%;">Search</button>
                    <a href="{{route('users.create')}}" class="">
                        <button type="button" class="btn btn-info col" style="width:25%;">Create</button>
                    </a>
                </div>
            </div>
        </div>
    </form>
    <div class="row mt-2">
        <div class="col-md-12 col-sm-12">
            <div class="x_panel">
                <div class="x_content">
    <table class="table table-striped">
        <thead>
            <tr>
                <th scope="col">STT</th>
                <th scope="col">ID</th>
                <th scope="col">Role</th>
                <th scope="col">Name</th>
                <th scope="col">Email</th>
                <th scope="col">Gender</th>
                <th scope="col">Birth day</th>
                <th scope="col">Address</th>
                <th scope="col">Identify Card</th>
                <th scope="col">Phone</th>
                <th scope="col">Status</th>
                @if(Auth::user()->role_id == 1)
                <th scope="col">Action</th>
                @endif
            </tr>
        </thead>
        <tbody>
            <?php $count = 1; ?>
            @foreach($users as $user)
            <tr>
                <th scope="row">{{$users->perPage()*($users->currentPage()-1)+$count}}</th>
                <td>{{$user->id}}</td>
                <td>{{$user->role_name}}</td>
                <td>{{$user->name}}</td>
                <td>{{$user->email}}</td>
                @if($user->gender == 1)
                <td>Man</td>
                @elseif($user->gender == 2)
                <td>Lady</td>
                @else
                <td></td>
                @endif
                <td>{{$user->birth_day}}</td>
                <td>{{$user->address}}</td>
                <td>{{$user->identify_card}}</td>
                <td>{{$user->phone}}</td>
                @if($user->status == 0)
                <td style="color:red; font-weight:bold;"><i class="fa fa-ban fa-3x" aria-hidden="true"></i></td>
                @else
                <td style="color:green;font-weight:bold;"><i class="fa fa-check-circle-o fa-3x" aria-hidden="true"></i></td>
                @endif
                @if(Auth::user()->role_id == 1)
                <td>
                    <a href="{{route('users.edit', $user->id) }}" style="color:blue;">
                        <svg width="1.5em" height="1.5em" viewBox="0 0 16 16" class="bi bi-pencil-square"
                            fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path
                                d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456l-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z" />
                            <path fill-rule="evenodd"
                                d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z" />
                        </svg>
                    </a>
                    <a href="" data-toggle="modal" data-target="#exampleModal" onClick="onClickDelete({{$user->id}})"
                        style="color:red;">
                        <svg width="1.5em" height="1.5em" viewBox="0 0 16 16" class="bi bi-trash" fill="currentColor"
                            xmlns="http://www.w3.org/2000/svg">
                            <path
                                d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z" />
                            <path fill-rule="evenodd"
                                d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4L4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z" />
                        </svg>
                    </a>
                </td>
                @endif
            </tr>

        </tbody>
        <?php $count++; ?>
        @endforeach
    </table>
    <form style="display:inline" action="" method="post" id="delete-form">
        @csrf
        @method('DELETE')
        <div class="modal" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content" style="border-radius: 1rem;">
                    <div class="modal-header" style="background-color: #2A3F54;">
                        <h5 class="modal-title" id="exampleModalLabel" style="color:white;">DELETE</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <h1 style="text-align:center;">Are you sure?</h1>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal"
                            style="width: 100px;">Close</button>
                        <button type="submit" class="btn btn-danger" data-toggle="modal" data-target="#successdelete"
                            onClick="submitdelete()" style="width: 67px;">Yes</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <div class="d-flex justify-content-end nav">
        {!! $users->withQueryString()->links('pagination.paginate') !!}
    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@if (Session::has('message'))
<div id="snackbar">{{ Session::get('message') }}</div>
<script>
    var x = document.getElementById("snackbar");
    x.className = "show";
    setTimeout(function() {
        x.className = x.className.replace("show", "");
    }, 3000);

</script>
</div>
@endif
<script>
const onClickDelete = (id) => {
    const deleteForm = document.getElementById('delete-form');
    deleteForm.action = '/users/' + id;
};

function submitdelete() {
    deleteForm.submit();
}
</script>

@endsection