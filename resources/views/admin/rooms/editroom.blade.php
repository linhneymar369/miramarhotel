@extends('adminLayout')
@section('content')
<style>
    .btn-file {
        position: relative;
        overflow: hidden;
    }
    .btn-file input[type=file] {
        position: absolute;
        top: 0;
        right: 0;
        min-width: 100%;
        min-height: 100%;
        font-size: 100px;
        text-align: right;
        filter: alpha(opacity=0);
        opacity: 0;
        outline: none;
        background: white;
        cursor: inherit;
        display: block;
    }
</style>    
<div class="right_col" style="min-height: 953px;">
    <div class="row mt-2">
        <div class="col-md-12 col-sm-12">
            <div class="x_panel">
                <div class="x_content">
    <h1 style="text-align: center;">Edit Room</h1>
    <form action="{{route('rooms.update',$room->id)}}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('PUT')
    <div class="form-group">
        <h2 for="">Image 1:</h2>
        <img src="{{ asset('images/rooms/'.$room->img1)}}"  id="output1" alt="avatar" width="600" height="400">
        <span class="btn btn-default btn-file" style="border: 1px solid cadetblue;background: aliceblue;">
            <input type="file" name="img1" id="file-upload" accept="image/*" onchange="loadFile1(event)" class="custom-file-input">
            <span id="file-name" class="custom-file-control">Choose file</span>
        </span>
    </div>
    <div class="form-group">
        <h2 for="">Image 2:</h2>
        <img src="{{ asset('images/rooms/'.$room->img2)}}"  id="output2" alt="avatar" width="600" height="400">
        <span class="btn btn-default btn-file" style="border: 1px solid cadetblue;background: aliceblue;">
            <input type="file" name="img2" id="file-upload" accept="image/*" onchange="loadFile2(event)" class="custom-file-input">
            <span id="file-name" class="custom-file-control">Choose file</span>
        </span>
    </div>
    <div class="form-group">
        <h2 for="">Image 3:</h2>
        <img src="{{ asset('images/rooms/'.$room->img3)}}"  id="output3" alt="avatar" width="600" height="400">
        <span class="btn btn-default btn-file" style="border: 1px solid cadetblue;background: aliceblue;">
            <input type="file" name="img3" id="file-upload" accept="image/*" onchange="loadFile3(event)" class="custom-file-input">
            <span id="file-name" class="custom-file-control">Choose file</span>
        </span>
    </div>
    <div class="form-group">
    <label for="">Type room:</label>
        <select name="type_id" id="" class="form-control">
            @if($room->type_id == 1)
            <option value="1">Standard Room</option>
            <option value="2">Intermediate Room</option>
            <option value="3">Luxury Room</option>
            <option value="4">VIP Room</option>
            <option value="5">Presidential Room</option>
            @elseif($room->type_id == 2)
            <option value="2">Intermediate Room</option>
            <option value="1">Standard Room</option>
            <option value="3">Luxury Room</option>
            <option value="4">VIP Room</option>
            <option value="5">Presidential Room</option>
            @elseif($room->type_id == 3)
            <option value="3">Luxury Room</option>
            <option value="1">Standard Room</option>
            <option value="2">Intermediate Room</option>
            <option value="4">VIP Room</option>
            <option value="5">Presidential Room</option>
            @elseif($room->type_id == 4)
            <option value="4">VIP Room</option>
            <option value="1">Standard Room</option>
            <option value="2">Intermediate Room</option>
            <option value="3">Luxury Room</option>
            <option value="5">Presidential Room</option>
            @elseif($room->type_id == 5)
            <option value="5">Presidential Room</option>
            <option value="1">Standard Room</option>
            <option value="2">Intermediate Room</option>
            <option value="3">Luxury Room</option>
            <option value="4">VIP Room</option>
            @endif
        </select>
    </div>
    <div class="form-group">
        <label for="">Description:</label>
    <textarea type="text" name="description" id="" class="form-control">{{$room->description}}</textarea>
    </div>
    <div class="form-group">
        <label for="">Amount people:</label>
        <select name="amount_user" id="" class="form-control">
            @if($room->amount_user == 1)
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
            @elseif($room->amount_user == 2)
            <option value="2">2</option>
            <option value="1">1</option>
            <option value="3">3</option>
            <option value="4">4</option>
            @elseif($room->amount_user == 3)
            <option value="3">3</option>
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="4">4</option>
            @elseif($room->amount_user == 4)
            <option value="4">4</option>
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            @endif
        </select>   
    </div>
    <div class="form-group">
        <label for="">Status:</label>
        <select name="status" id="" class="form-control">
            @if($room->status == 1)
            <option value="1">Active</option>
            <option value="2">Close</option>
            @elseif ($room->status == 2)
            <option value="2">Close</option>
            <option value="1">Active</option>
            @endif
        </select>
        <button class="btn btn-primary" style="margin-left: 45%;margin-top: 20px;"  >Submit</button>
    </div>
    
</div>
            </div>
        </div>
    </div>
</div>
</form>
@if ($errors->any())
            <div id="snackbar">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
                <script>
                    var x = document.getElementById("snackbar");
                    x.className = "show";
                    setTimeout(function() {
                        x.className = x.className.replace("show", "");
                    }, 3000);
                </script>
            <br />
        @endif
<script>
    var loadFile1 = function (event) {
    var output = document.getElementById("output1");
    output.src = URL.createObjectURL(event.target.files[0]);
    output.onload = function () {
        URL.revokeObjectURL(output.src); 
    };
};
var loadFile2 = function (event) {
    var output = document.getElementById("output2");
    output.src = URL.createObjectURL(event.target.files[0]);
    output.onload = function () {
        URL.revokeObjectURL(output.src);
    };
};
var loadFile3 = function (event) {
    var output = document.getElementById("output3");
    output.src = URL.createObjectURL(event.target.files[0]);
    output.onload = function () {
        URL.revokeObjectURL(output.src); 
    };
};
</script>
@endsection