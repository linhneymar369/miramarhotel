@extends('adminLayout')
@section('content')
<style>
.btn-file {
    position: relative;
    overflow: hidden;
}
.btn-file input[type=file] {
    position: absolute;
    top: 0;
    right: 0;
    min-width: 100%;
    min-height: 100%;
    font-size: 100px;
    text-align: right;
    filter: alpha(opacity=0);
    opacity: 0;
    outline: none;
    background: white;
    cursor: inherit;
    display: block;
}

#img-upload-1{
    width: 100%;
}
#img-upload-2{
    width: 100%;
}
#img-upload-3{
    width: 100%;
}
</style>
<div class="right_col" style="min-height: 953px;">
    <div class="row mt-2">
        <div class="col-md-12 col-sm-12">
            <div class="x_panel">
                <div class="x_content">
<h1 style="text-align: center;">Create Room</h1>
<form action="{{route('rooms.store')}}" method="post" enctype="multipart/form-data">
@csrf       
    <div class="form-group">
        <div class="form-row" style="margin-top: 80px;">
            <div class="col">
                    <label>Image 1</label>
                <div class="input-group">
                    <span class="input-group-btn">
                        <span class="btn btn-default btn-file">
                            Browse… <input type="file" id="imgInp-1" name="img1">
                        </span>
                    </span>
                    <input type="text" class="form-control" readonly >
                </div>
                <img id='img-upload-1'/>
            </div>
            <div class="col">
                    <label>Image 2</label>
                <div class="input-group">
                    <span class="input-group-btn">
                        <span class="btn btn-default btn-file">
                            Browse… <input type="file" id="imgInp-2" name="img2">
                        </span>
                    </span>
                    <input type="text" class="form-control" readonly >
                </div>
                <img id='img-upload-2'/>
            </div>
            <div class="col">
                    <label>Image 3</label>
                <div class="input-group">
                    <span class="input-group-btn">
                        <span class="btn btn-default btn-file">
                            Browse… <input type="file" id="imgInp-3" name="img3">
                        </span>
                    </span>
                    <input type="text" class="form-control" readonly >
                </div>
                <img id='img-upload-3'/>
            </div>
        </div>
        <label for="">Type room:</label>
        <select name="type_id" id="" class="form-control">
            <option value="1">Standard Room</option>
            <option value="2">Intermediate Room</option>
            <option value="3">Luxury Room</option>
            <option value="4">VIP Room</option>
            <option value="5">Presidential Room</option>
        </select>
        <label for="">Description:</label>
        <textarea type="text" name="description" id="" class="form-control"></textarea>
        <label for="">Amount people:</label>
        <select name="amount_user" id="" class="form-control">
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
        </select>   
        <label for="">Status:</label>
        {{-- <input type="number" name="status" id="" class="form-control"> --}}
        <select name="status" id="" class="form-control">
            <option value="1">Active</option>
            <option value="2">Close</option>
        </select>
        <button class="btn btn-primary" style="margin-left: 45%;margin-top: 20px;">Submit</button>
    </div>
    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@if ($errors->any())
            <div id="snackbar">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
                <script>
                    var x = document.getElementById("snackbar");
                    x.className = "show";
                    setTimeout(function() {
                        x.className = x.className.replace("show", "");
                    }, 5000);
                </script>
            <br />
        @endif
<script>
$(document).ready( function() {
    $(document).on('change', '.btn-file :file', function() {
		var input = $(this),
			label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
		input.trigger('fileselect', [label]);
		});

		$('.btn-file :file').on('fileselect', function(event, label) {
		var input = $(this).parents('.input-group').find(':text'),
		log = label;
		if( input.length ) {
		input.val(log);
		} else {
		if( log ) alert(log);
		}
		});
		function readURL(input) {
		if (input.files && input.files[0]) {
		var reader = new FileReader();
		reader.onload = function (e) {
		$('#img-upload-1').attr('src', e.target.result);
		}
		reader.readAsDataURL(input.files[0]);
		}
		}
		$("#imgInp-1").change(function(){
		readURL(this);
		}); 	
	});
    // ------------------------
    $(document).ready( function() {
    $(document).on('change', '.btn-file :file', function() {
		var input = $(this),
			label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
		input.trigger('fileselect', [label]);
		});

		$('.btn-file :file').on('fileselect', function(event, label) {
		var input = $(this).parents('.input-group').find(':text'),
		log = label;
		if( input.length ) {
		input.val(log);
		} else {
		if( log ) alert(log);
		}
		});
		function readURL(input) {
		if (input.files && input.files[0]) {
		var reader = new FileReader();
		reader.onload = function (e) {
		$('#img-upload-2').attr('src', e.target.result);
		}
		reader.readAsDataURL(input.files[0]);
		}
		}

		$("#imgInp-2").change(function(){
	    readURL(this);
		}); 	
	});
    // -----------------
    $(document).ready( function() {
    	$(document).on('change', '.btn-file :file', function() {
		var input = $(this),
			label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
		input.trigger('fileselect', [label]);
		});

		$('.btn-file :file').on('fileselect', function(event, label) {
		    var input = $(this).parents('.input-group').find(':text'),
		        log = label;
		    
		    if( input.length ) {
		        input.val(log);
		    } else {
		        if( log ) alert(log);
		    }
	    
		});
		function readURL(input) {
		    if (input.files && input.files[0]) {
		        var reader = new FileReader();
		        
		        reader.onload = function (e) {
		            $('#img-upload-3').attr('src', e.target.result);
		        }
		        
		        reader.readAsDataURL(input.files[0]);
		    }
		}

		$("#imgInp-3").change(function(){
		    readURL(this);
		}); 	
	});
</script>
@endsection