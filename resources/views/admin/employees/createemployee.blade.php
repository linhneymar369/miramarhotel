@extends('adminLayout')
@section('content')
<div class="right_col" style="min-height: 953px;">
    <div class="row mt-2">
        <div class="col-md-12 col-sm-12">
            <div class="x_panel">
                <div class="x_content">
<h1 style="text-align: center;">Create Employee</h1>
<form action="{{route('employees.store')}}" method="post">
@csrf       

@endif
    <div class="form-group">
        <label for="">Email:</label>
        <input type="text" name="email" id="" class="form-control">
        <label for="">Password:</label>
        <input type="text" name="password" id="" class="form-control">
        <label for="">Name:</label>
        <input type="text" name="name" id="" class="form-control">
        <label for="">Gender:</label>
        <select name="gender" id="" class="form-control">
            <option value="2">Lady</option>
            <option value="1">Man</option>
        </select>
        <label for="">Birth day:</label>
        <input type="date" name="birth_day" id="" class="form-control">
        <label for="">Address:</label>
        <input type="text" name="address" id="" class="form-control">
        <label for="">Identify Card:</label>
        <input type="text" name="identify_card" id="" class="form-control">
        <label for="">Phone:</label>
        <input type="text" name="phone" id="" class="form-control">
        <label for="">Status:</label>
        <select class="form-control" name="status" id="">
            <option value="0">Lock</option>
            <option value="1">Active</option>
        </select>
        <button class="btn btn-primary" style="margin-left: 45%;margin-top: 20px;">Submit</button>
    </div>
    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@if ($errors->any())
            <div id="snackbar">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
                <script>
                    var x = document.getElementById("snackbar");
                    x.className = "show";
                    setTimeout(function() {
                        x.className = x.className.replace("show", "");
                    }, 5000);
                </script>
            <br />
        @endif
@endsection