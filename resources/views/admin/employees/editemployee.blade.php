@extends('adminLayout')
@section('content')
<div class="right_col" style="min-height: 953px;">
    <div class="row mt-2">
        <div class="col-md-12 col-sm-12">
            <div class="x_panel">
                <div class="x_content">
    <h1 style="text-align: center;">Edit Employee</h1>
    <form action="{{route('employees.update',$employee)}}" method="post">
        @method('put')
        @csrf
        <div class="form-group">
            <label for="">Email:</label>
            <input type="text" name="email" id="" class="form-control" value="{{$employee->email}}">
            <label for="">Name:</label>
            <input type="text" name="name" id="" class="form-control" value="{{$employee->name}}">
            <label for="">Gender:</label>
            <select name="gender" id="" class="form-control">
                @if($employee->gender == 2)
                <option value="2">Lady</option>
                <option value="1">Man</option>
                @else
                <option value="1">Man</option>
                <option value="2">Lady</option>
                @endif
            </select>
            <label for="">Birth day:</label>
            <input type="date" name="birth_day" id="" class="form-control" value="{{$employee->birth_day}}">
            <label for="">Address:</label>
            <input type="text" name="address" id="" class="form-control" value="{{$employee->address}}">
            <label for="">Identify Card:</label>
            <input type="text" name="identify_card" id="" class="form-control" value="{{$employee->identify_card}}">
            <label for="">Phone:</label>
            <input type="text" name="phone" id="" class="form-control" value="{{$employee->phone}}">
            <label for="">Status:</label>
            <select class="form-control" name="status" id="">
                @if($employee->status == 0)
                <option value="0">Lock</option>
                <option value="1">Active</option>
                @else($employee->status == 1)
                <option value="1">Active</option>
                <option value="0">Lock</option>
                @endif
            </select>
            <button class="btn btn-primary" style="margin-left: 45%;margin-top: 20px;">Submit</button>
        </div>
    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@if ($errors->any())
            <div id="snackbar">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
                <script>
                    var x = document.getElementById("snackbar");
                    x.className = "show";
                    setTimeout(function() {
                        x.className = x.className.replace("show", "");
                    }, 3000);
                </script>
            <br />
        @endif
@endsection