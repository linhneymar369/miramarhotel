@extends('adminLayout')
@section('content')
<div class="right_col" style="min-height: 953px;">
    <h1 style="text-align: center;">List Message</h1>
    <div class="row mt-2">
        <div class="col-md-12 col-sm-12">
            <div class="x_panel">
                <div class="x_content">
    <table class="table table-striped">
        <thead>
            <tr>
                <th scope="col">STT</th>
                <th scope="col">Name</th>
                <th scope="col">Email</th>
                <th scope="col">Subject</th>
                <th scope="col">Message</th>
                <th scope="col">Time</th>
            </tr>
        </thead>
        <tbody>
            <?php $count = 1; ?>
            @foreach ($messages as $message)
            <tr>
            <th scope="row">{{$messages->perPage()*($messages->currentPage()-1)+$count}}</th>
            <td>{{$message->name}}</td>
                <td>{{$message->mail}}</td>
                <td>{{$message->subject}}</td>
                <td>{{$message->message}}</td>
                <td>{{$message->created_at}}</td>
            </tr>
            <?php $count++; ?>
            @endforeach
        </tbody>
    </table>
    <div class="d-flex justify-content-end nav">
        {!! $messages->withQueryString()->links('pagination.paginate') !!}
    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection