@extends ('adminLayout')
@section('content')
<div class="right_col" role="main">
<div class="row mt-2">
    <div class="col-md-12 col-sm-12">
        <div class="x_panel">
            <div class="x_content">
                <div class="col-sm-8 offset-sm-2">
                    <div class="container" style="min-height: 800px;">
                        @if (session('message'))
                        <div class="alert alert-success section" style="text-align: center;margin-left: 40%;width: 200px;">
                            {{ session('message') }}
                        </div>
                        @endif
                        <div class="row my-2">
                            <div class="col-lg-8 order-lg-2">
                                <ul class="nav nav-tabs">
                                    <li class="nav-item">
                                        <a href="" data-target="#profile" data-toggle="tab" class="nav-link active">Profile</a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="" data-target="#edit_info" data-toggle="tab" class="nav-link">Edit Profile</a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="" data-target="#change_password" data-toggle="tab" class="nav-link">Change Password</a>
                                    </li>
                                </ul>
                                <div class="tab-content py-4">
                                    <div class="tab-pane active" id="profile">
                                        <h5 class="mb-3" style="text-align: center;">User Profile</h5>
                                        <hr>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <h6>Name</h6>
                                                <p style="font-size: 18px">
                                                    {{$data->name}}
                                                </p>
                                                <h6>Email</h6>
                                                <p style="font-size: 18px">
                                                    {{$data->email}}
                                                </p>
                                            </div>
                                            <div class="col-md-6">
                                                
                                                <h6>Address</h6>
                                                @if ($data->address == null)
                                                <p style="font-size: 18px">Null</p>
                                                @else
                                                <p style="font-size: 18px">{{$data->address}}</p>
                                                @endif
                                                <h6>Identify Card</h6>
                                                @if ($data->address == null)
                                                <p style="font-size: 18px">Null</p>
                                                @else
                                                <p style="font-size: 18px">{{$data->identify_card}}</p>
                                                @endif
                                            </div>
                                        </div>
                                        <!--/row-->
                                    </div>
                                    <div class="tab-pane" id="edit_info">
                                        <form action="{{route('updateprofile')}}" method="POST" role="form">
                                            @csrf
                                            @method('put')
                                            <div class="form-group row">
                                                <label class="col-lg-3 col-form-label form-control-label">Name</label>
                                                <div class="col-lg-9">
                                                    <input class="form-control" name="name" type="text" value="{{$data->name}}">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-lg-3 col-form-label form-control-label">Gender</label>
                                                <div class="col-lg-9">
                                                    <select name="gender" id="" class="form-control">
                                                    @if($data->gender == 1)
                                                    <option value="1">Man</option>
                                                    <option value="2">Lady</option>
                                                    @else 
                                                    <option value="2">Lady</option>
                                                    <option value="1">Man</option>
                                                    @endif
                                                </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-lg-3 col-form-label form-control-label">Birth day</label>
                                                <div class="col-lg-9">
                                                    <input type="date" name="birth_day" class="form-control" type="text" value="{{$data->birth_day}}">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-lg-3 col-form-label form-control-label">Address</label>
                                                <div class="col-lg-9">
                                                    <input class="form-control" name="address" value="{{$data->address}}">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-lg-3 col-form-label form-control-label">Identify Card</label>
                                                <div class="col-lg-9">
                                                    <input class="form-control" name="identify_card" value="{{$data->identify_card}}">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-lg-3 col-form-label form-control-label">Phone</label>
                                                <div class="col-lg-9">
                                                    <input class="form-control" name="phone" type="number" value="{{$data->phone}}">
                                                </div>
                                            </div>
                                           
                                            <div class="form-group row">
                                                <label class="col-lg-3 col-form-label form-control-label"></label>
                                                <div class="col-lg-9">
                                                    <button type="submut" class="btn btn-primary">Save Changes</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="tab-pane" id="change_password">
                                    <form action="{{route('changepassadmin')}}" method="POST" role="form">
                                            @csrf
                                            @method('put')
                                            <div class="form-group row">
                                                <label class="col-lg-3 col-form-label form-control-label">Old Password</label>
                                                <div class="col-lg-9">
                                                    <input class="form-control" name="oldPassword" type="text" value="">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-lg-3 col-form-label form-control-label">New Password</label>
                                                <div class="col-lg-9">
                                                    <input class="form-control" name="newPassword" type="password" value="">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-lg-3 col-form-label form-control-label">Confirm New Password</label>
                                                <div class="col-lg-9">
                                                    <input class="form-control" name="confirmPassword" type="password" value="">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-lg-3 col-form-label form-control-label"></label>
                                                <div class="col-lg-9">
                                                    
                                                    <button type="submit" class="btn btn-primary">Save Changes</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 order-lg-1 text-center">
                            <form action="{{route('changeavatar')}}" method="POST" enctype="multipart/form-data" id="changeAvatar">
                                    @csrf
                                    @method('PUT')
                                <img src=" @if($data->avatar !== null) {{asset('images/avatar/'.$data->avatar)}}
                            @else
                           {{asset('images/user.png')}}
                            @endif" class="mx-auto img-fluid img-circle d-block rounded-circle" id="output" alt="avatar" style="border-radius: 50%; width: 10rem; height: 10rem;">
                                <h6 class="mt-2">Upload your avatar</h6>
                                <label class="custom-file">
                                    <input type="file" name="avatar" id="file-upload" accept="image/*" onchange="loadFile(event)" class="custom-file-input">
                                    <span id="file-name" class="custom-file-control">Choose file</span>
                                </label>
                                <br>
                                <hr>
                    
                                  <div class="form-group row">
                                    <label class="col-lg-3 col-form-label form-control-label"></label>
                                    <div class="col-lg-6">
                                        <button type="submit" class="btn btn-primary">Save Avatar</button>
                                    </div>
                                </div>
                            </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<script>
    var loadFile = function (event) {
    var output = document.getElementById("output");
    output.src = URL.createObjectURL(event.target.files[0]);
    output.onload = function () {
        URL.revokeObjectURL(output.src); // free memory
    };
};

</script>
@endsection