<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<link rel="icon" href="images/logo_3.png" type="image/png" />

<head>
    <title>Marimar</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="Marimar Hotel template project">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="styles/bootstrap-4.1.2/bootstrap.min.css">
    <link href="{{ asset('plugins/font-awesome-4.7.0/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/OwlCarousel2-2.3.4/owl.carousel.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/OwlCarousel2-2.3.4/owl.theme.default.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/OwlCarousel2-2.3.4/animate.css') }}">
    <link href="{{ asset('plugins/jquery-datepicker/jquery-ui.css') }}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="{{ asset('styles/main_styles.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('styles/responsive.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('styles/custom-navbar.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('styles/bootstrap.css') }}">
    <link href="{{ asset('styles/ekko-lightbox.css') }}" rel="stylesheet">
    <link href="{{ asset('styles/bootstrap-datetimepicker.min.css') }}" rel="stylesheet" media="screen">
    <link href="{{ asset('styles/datepicker.css') }}" rel="stylesheet" media="screen">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
        integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <script src="https://code.jquery.com/jquery-3.2.1.min.js"
        integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.2.1.js"
        integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE=" crossorigin="anonymous"></script>
    <style>
        #snackbar {
            visibility: hidden;
            min-width: 250px;
            margin-left: -125px;
            background-color: rgb(53, 54, 54);
            color: #fff;
            text-align: center;
            border-radius: 2px;
            padding: 16px;
            position: fixed;
            z-index: 100000;
            left: 50%;
            bottom: 30px;
            font-size: 17px;
        }

        #snackbar.show {
            visibility: visible;
            -webkit-animation: fadein 0.5s, fadeout 0.5s 2.5s;
            animation: fadein 0.5s, fadeout 0.5s 2.5s;
        }

        @-webkit-keyframes fadein {
            from {
                bottom: 0;
                opacity: 0;
            }

            to {
                bottom: 30px;
                opacity: 1;
            }
        }

        @keyframes fadein {
            from {
                bottom: 0;
                opacity: 0;
            }

            to {
                bottom: 30px;
                opacity: 1;
            }
        }

        @-webkit-keyframes fadeout {
            from {
                bottom: 30px;
                opacity: 1;
            }

            to {
                bottom: 0;
                opacity: 0;
            }
        }

        @keyframes fadeout {
            from {
                bottom: 30px;
                opacity: 1;
            }

            to {
                bottom: 0;
                opacity: 0;
            }
        }

        #back-to-top {
            position: fixed;
            right: 30px;
            bottom: 60px;
            z-index: 9999;
        }

        #back-to-top a {
            text-decoration: none;
        }

        #back-to-top a img {
            width: 40px;
            height: auto;
        }

    </style>
</head>
<header>
    <div id="menuSm" class="navbar navbar-fixed-top  navbar-inverse">
        <div style="position: relative;left:-25%">
            <a href="http://127.0.0.1:8000/locale/vn"><img src="{{ asset('images/logovn.webp') }}" alt=""
                    style="max-width: 25px;"></a>
            <a href="http://127.0.0.1:8000/locale/jp"><img src="{{ asset('images/logojapan.svg') }}" alt=""
                    style="max-width: 25px;"></a>
            <a href="http://127.0.0.1:8000/locale/en"><img src="{{ asset('images/logouk.png') }}" alt=""
                    style="max-width: 25px;"></a>
        </div>
        <div style="position: relative;left: 25%;">
            @if (Auth::check() === true)
                <button style="border-radius: 13%;background: none;border: none;" class="btn btn-danger"
                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <div>
                        @if (!empty(Auth::user()->avatar))
                            <img src="{{ asset('images/avatar/' . Auth::user()->avatar) }}" alt=""
                                style="height:2rem; border-radius:50%; width: 2rem;">
                        @else
                            <img src="{{ asset('images/user.png') }}" alt="" style="height:2rem; border-radius:50%">
                        @endif
                    </div>
                </button>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="/user_profile">{{ __('sentence.profile') }}</a>
                    <a class="dropdown-item" href="/history">{{ __('sentence.history') }}</a>
                    <div class="dropdown-divider"></div>
                    <a type="button" class="dropdown-item" href="/logout">{{ __('sentence.logout') }}</a>
                </div>
                <a href="{{ route('cart') }}"><i class="fa fa-shopping-cart" style="font-size: 1.5em;"
                        aria-hidden="true"></i></a>
            @else
                <button style="border-radius: 13%;background: none;border: none;" class="btn btn-danger"
                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <div>
                        <a href="">
                            <img src="{{ asset('images/user.png') }}" alt="" style="height:2rem; border-radius:50%">
                        </a>
                    </div>
                </button>
                <div class="dropdown-menu">
                    <a class="dropdown-item" data-toggle="collapse" data-target="#Login">{{ __('sentence.login') }}</a>
                    <a class="dropdown-item" data-toggle="collapse"
                        data-target="#SignUp">{{ __('sentence.signup') }}</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" data-toggle="collapse"
                        data-target="#Forgotpass">{{ __('sentence.forgetpassword') }}</a>
                </div>
            @endif
        </div>
    </div>

    <!-- Form đăng nhập -->
    <div class="modal fade" id="Login" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button class="close" data-toggle="collapse" data-target="#Login" type="button">×</button>
                </div>
                <form action="/send_login" method="post" id="form-login">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <div class="col-md-12">
                                <div id="ErrorMessage" style="background-color:red;color:#fff;"></div>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label class="control-label" for="">{{ __('sentence.email') }}:</label>
                                        <input id="" name="email" placeholder="{{ __('sentence.enteryouremail') }}"
                                            type="text" class="form-control input">
                                    </div>
                                    <div class="col-md-12">
                                        <label class="control-label" for="">{{ __('sentence.password') }}:</label>
                                        <input name="password" id=""
                                            placeholder="{{ __('sentence.enteryourpassword') }}" type="password"
                                            class="form-control input">
                                    </div>
                                    <input type="checkbox" name="remember_me" id=""
                                        style="margin-left:20px;"><span>{{ __('sentence.rememberme') }}</span>
                                    <a href="" data-toggle="collapse" data-target="#Forgotpass"
                                        style="margin-left: 100px;">{{ __('sentence.forgetpassword') }}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btnLoginModal button"
                            style="cursor: pointer;">{{ __('sentence.login') }}</button>
                        <button type="button" data-toggle="collapse" data-target="#SignUp" name="btnSignUp"
                            class="btnLoginModal button" style="cursor: pointer;">{{ __('sentence.signup') }}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Form đăng kí -->
    <div class="modal fade" id="SignUp" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button class="close" data-toggle="collapse" data-target="#SignUp" type="button">×</button>
                </div>
                <form action="/send_register" method="post" id="form-signup">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <div class="col-md-12">
                                <div id="ErrorMessage" style="background-color:red;color:#fff;"></div>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label class="control-label" for="">{{ __('sentence.email') }}:</label>
                                        <input id="" name="email" placeholder="{{ __('sentence.enteryouremail') }}"
                                            type="text" class="form-control input">
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <label class="control-label" for="">{{ __('sentence.fullname') }}:</label>
                                            <input id="" name="name"
                                                placeholder="{{ __('sentence.enteryourfullname') }}" type="text"
                                                class="form-control input">
                                        </div>
                                        <div class="col-md-12">
                                            <label class="control-label" for="">{{ __('sentence.password') }}:</label>
                                            <input name="password" id=""
                                                placeholder="{{ __('sentence.enteryourpassword') }}" type="password"
                                                class="form-control input ">
                                        </div>
                                        <div class="col-md-12">
                                            <label class="control-label"
                                                for="">{{ __('sentence.confirmpassword') }}:</label>
                                            <input name="re_password" id=""
                                                placeholder="{{ __('sentence.reenteryourpassword') }}" type="password"
                                                class="form-control input">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" name="" class="btnLoginModal button"
                                style="cursor: pointer;">{{ __('sentence.signup') }}</button>
                        </div>
                </form>
            </div>
        </div>
    </div>
    </div>
    <!-- Form forgot password -->
    <div class="modal fade" id="Forgotpass" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button class="close" data-toggle="collapse" data-target="#Forgotpass" type="button">×</button>
                </div>
                <form action="/send_email_password" method="POST">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <div class="col-md-12">
                                <div id="ErrorMessage" style="background-color:red;color:#fff;"></div>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label class="control-label" for="">{{ __('sentence.email') }}:</label>
                                        <input id="" name="email" placeholder="{{ __('sentence.enteryouremail') }}"
                                            type="text" class="form-control input">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btnforgotpass button"
                            style="cursor: pointer;">{{ __('sentence.Submit') }}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    </div>
</header>

<body>
    <br />
    <div class="super_container">
        @if ($errors->any())
            <div id="snackbar">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
                <script>
                    var x = document.getElementById("snackbar");
                    x.className = "show";
                    setTimeout(function() {
                        x.className = x.className.replace("show", "");
                    }, 5000);

                </script>
            <br />
        @endif
        @if (Session::has('message'))
                <div id="snackbar">{{ Session::get('message') }}</div>
                <script>
                    var x = document.getElementById("snackbar");
                    x.className = "show";
                    setTimeout(function() {
                        x.className = x.className.replace("show", "");
                    }, 4000);

                </script>
            </div>
        @endif
        <header class="header">
            <div
                class="header_content d-flex flex-column align-items-center justify-content-lg-end justify-content-center">
                <!-- Logo -->
                <div class="logo">
                    <a href="{{ route('home') }}"><img class="logo_1" src="{{ asset('images/logo.png') }}" alt=""><img
                            class="logo_2" src="{{ asset('images/logo_2.png') }}" alt=""><img class="logo_3"
                            src="{{ asset('images/logo_3.png') }}" alt=""></a>
                </div>
                <!-- Main Nav -->
                <nav class="main_nav">
                    <ul class="d-flex flex-row align-items-center justify-content-start">
                        <li><a href="/">{{ __('sentence.home') }}</a></li>
                        <li><a href="{{ route('aboutus') }}">{{ __('sentence.aboutus') }}</a></li>
                        <li><a href="{{ route('room') }}">{{ __('sentence.rooms') }}</a></li>
                        <li><a href="{{ route('contact') }}">{{ __('sentence.contact') }}</a></li>
                    </ul>
                </nav>
                <!-- Social -->
                <div class="social header_social">
                    <ul class="d-flex flex-row align-items-center justify-content-start">
                        <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>
                    </ul>
                </div>
                <!-- Header Right -->
                <div class="header_right d-flex flex-row align-items-center justify-content-start">
                    <!-- Header Link -->
                    <div class="header_link"><a href="{{ route('cart') }}">{{ __('sentence.yourbookingcart') }}</a>
                    </div>
                    <!-- Hamburger Button -->
                    <div class="hamburger"><i class="fa fa-bars" aria-hidden="true"></i></div>
                </div>
                <!-- Search Panel -->
                <div class="search_panel">
                    <div class="search_panel_content d-flex flex-row align-items-center justify-content-start">
                        <img src="{{ asset('images/search.png') }}" alt="">
                        <form action="#" class="search_form">
                            <input type="text" class="search_input" placeholder="Type your search here"
                                required="required">
                        </form>
                        <div class="search_close ml-auto d-flex flex-column align-items-center justify-content-center">
                            <div></div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!-- Logo Overlay -->
        <div class="logo_overlay" style="margin-top: 50px;background: none;">
            <div class="logo_overlay_content d-flex flex-column align-items-center justify-content-center">
                <div class="logo">
                    <a href="{{ route('home') }}"><img src="{{ asset('images/logo_3.png') }}" alt=""></a>
                </div>
            </div>
        </div>
        <!-- Menu Overlay -->
        <div class="menu_overlay" style="margin-top: 50px;background: none;">
            <div class="menu_overlay_content d-flex flex-row align-items-center justify-content-center">
                <!-- Hamburger Button -->
                <div class="hamburger"><i class="fa fa-bars" aria-hidden="true"></i></div>
            </div>
        </div>
        <script type="text/javascript">
            $(window).on('load', function() {
                $('#modalSuccess').modal('show');
            });

        </script>
        <div class="menu">
            <div class="menu_container d-flex flex-column align-items-center justify-content-center">
                <!-- Menu Navigation -->
                <nav class="menu_nav text-center">
                    <ul>
                        <li><a href="{{ route('home') }}">{{ __('sentence.home') }}</a></li>
                        <li><a href="{{ route('aboutus') }}">{{ __('sentence.aboutus') }}</a></li>
                        <li><a href="{{ route('room') }}">{{ __('sentence.rooms') }}</a></li>
                        <li><a href="{{ route('contact') }}">{{ __('sentence.contact') }}</a></li>
                    </ul>
                </nav>
                <div class="button menu_button"><a href="{{ route('room') }}">{{ __('sentence.booknow') }}</a></div>
                <!-- Menu Social -->
                <div class="social menu_social">
                    <ul class="d-flex flex-row align-items-center justify-content-start">
                        <li><a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-behance" aria-hidden="true"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- Home -->
        <div class="home">
            <div class="parallax_background parallax-window" data-parallax="scroll"
                data-image-src="{{ asset('images/home.jpg') }}" data-speed="0.8"></div>
            <div class="home_container d-flex flex-column align-items-center justify-content-center">
                <div class="home_title">
                    <h1>{{ __('sentence.bookyourstay') }}</h1>
                </div>
                <div class="home_text text-center">{{ __('sentence.bookyourstaydown') }}
                </div>
                <div class="button home_button"><a href="{{ route('room') }}">{{ __('sentence.booknow') }}</a></div>
            </div>
        </div>
        <!-- Booking -->
        <div class="booking">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <div class="booking_container d-flex flex-row align-items-end justify-content-start"
                            style="border-radius: 100px 100px 1px 1px;">
                            <form action="{{ route('room') }}" method="get" class="booking_form" autocomplete="off">
                                <div
                                    class="booking_form_container d-flex flex-lg-row flex-column align-items-start justify-content-start flex-wrap">
                                    <div
                                        class="booking_form_inputs d-flex flex-row align-items-start justify-content-between flex-wrap">
                                        <div class="booking_dropdown"><input type="text"
                                                class="datepicker booking_input booking_input_a booking_in"
                                                placeholder="{{ __('sentence.Check In') }}" name="check_in" value="">
                                        </div>
                                        <div class="booking_dropdown"><input type="text"
                                                class="datepicker booking_input booking_input_a booking_out"
                                                placeholder="{{ __('sentence.Check Out') }}" name="check_out" value="">
                                        </div>
                                        <div class="custom-select">
                                            <select name="amount_user" id="person">
                                                <option value="">{{ __('sentence.People') }}</option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                            </select>
                                        </div>
                                        <div class="custom-select" value="">
                                            <select name="type" id="person">
                                                <option value="">{{ __('sentence.Type Room') }}</option>
                                                <option value="1">Standard Room</option>
                                                <option value="2">Intermediate Room</option>
                                                <option value="3">Luxury Room</option>
                                                <option value="4">VIP Room</option>
                                                <option value="5">Presidential Room</option>
                                            </select>
                                        </div>
                                    </div>
                                    <button type="submit"
                                        class="booking_form_button ml-lg-auto">{{ __('sentence.booknow') }}</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="rooms">
            <div class="container">
                @yield('home')
                @yield('content')
            </div>
        </div>
        <footer class="footer">
            <div class="parallax_background parallax-window" data-parallax="scroll" data-image-src="images/footer.jpg"
                data-speed="0.8"></div>
            <div class="container">
                <div class="row">
                    <div class="col">
                        <div class="footer_logo text-center">
                            <a href="#"><img src="{{ asset('images/logo.png') }}" alt=""></a>
                        </div>
                        <div class="footer_content">
                            <div class="row">
                                <div class="col-lg-4 footer_col">
                                    <div
                                        class="footer_info d-flex flex-column align-items-lg-end align-items-center justify-content-start">
                                        <div class="text-center">
                                            <div>Phone:</div>
                                            <div>+84 372 563 963</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 footer_col">
                                    <div
                                        class="footer_info d-flex flex-column align-items-center justify-content-start">
                                        <div class="text-center">
                                            <div>Address:</div>
                                            <div>1 Central Park West, New York, NY 10023, American</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 footer_col">
                                    <div
                                        class="footer_info d-flex flex-column align-items-lg-start align-items-center justify-content-start">
                                        <div class="text-center">
                                            <div>Mail:</div>
                                            <div>contact@miramarhotel.com</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="footer_bar text-center">
                            Copyright &copy;
                            <script>
                                document.write(new Date().getFullYear());

                            </script> <span style="color:yellow;"> Le Nhat Linh </span>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>
    <script src="{{ asset('js/jquery-3.3.1.min.js') }}"></script>
    <script src="{{ asset('styles/bootstrap-4.1.2/popper.js') }}"></script>
    <script src="{{ asset('styles/bootstrap-4.1.2/bootstrap.min.js') }}"></script>
    <script src="{{ asset('plugins/greensock/TweenMax.min.js') }}"></script>
    <script src="{{ asset('plugins/greensock/TimelineMax.min.js') }}"></script>
    <script src="{{ asset('plugins/scrollmagic/ScrollMagic.min.js') }}"></script>
    <script src="{{ asset('plugins/greensock/animation.gsap.min.js') }}"></script>
    <script src="{{ asset('plugins/greensock/ScrollToPlugin.min.js') }}"></script>
    <script src="{{ asset('plugins/OwlCarousel2-2.3.4/owl.carousel.js') }}"></script>
    <script src="{{ asset('plugins/easing/easing.js') }}"></script>
    <script src="{{ asset('plugins/progressbar/progressbar.min.js') }}"></script>
    <script src="{{ asset('plugins/parallax-js-master/parallax.min.js') }}"></script>
    <script src="{{ asset('plugins/jquery-datepicker/jquery-ui.js') }}"></script>
    <script src="{{ asset('js/ekko-lightbox.js') }}"></script>
    <script src="{{ asset('js/custom.js') }}"></script>
    <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js" type="text/javascript">
    </script>
    <script>
        jQuery(document).ready(function($) {
            if ($(window).scrollTop() > 200) {
                $('#back-to-top').fadeIn();
            } else {
                $('#back-to-top').fadeOut();
            }
            $(window).scroll(function() {
                if ($(this).scrollTop() > 200) {
                    $('#back-to-top').fadeIn();
                } else {
                    $('#back-to-top').fadeOut();
                }
            });
            $('#back-to-top').click(function() {
                $("html, body").animate({
                    scrollTop: 0
                }, 600);
                return false;
            });
        });
        $("#form-login").validate({
            rules: {
                email: {
                    required: true,
                },
                password: {
                    required: true,
                }
            },
            errorElement: 'span',
            errorPlacement: function(error, element) {
                error.addClass('invalid-feedback');
            },
            highlight: function(element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).removeClass('is-invalid');
            }
        });
        $("#form-signup").validate({
            rules: {
                email: {
                    required: true,
                },
                name: {
                    required: true,
                },
                password: {
                    required: true,
                },
                re_password: {
                    required: true,
                }
            },
            errorElement: 'span',
            errorPlacement: function(error, element) {
                error.addClass('invalid-feedback');
            },
            highlight: function(element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).removeClass('is-invalid');
            }
        });
        $(".alert").delay(4000).slideUp(200, function() {
            $(this).alert('close');
        });

    </script>
    <a id="back-to-top" href="#" style="position: fixed;right: 30px;bottom: 60px;z-index: 9999;">
        <img src="{{ asset('images/arr.png') }}">
    </a>
</body>
