@extends('base')
@section('content')
<div class="container">
    <form action="/change-password-with-token" method="post">
        @csrf
        <div class="form-group">
            @if (Session('message_error'))
                <div class="alert alert-danger text-center">{{Session::get('message_error')}}</div>
                @endif

                @if (Session('message_success'))
                <div class="alert alert-success text-center">{{Session::get('message_success')}}</div>
                @endif

                @if ($errors->any())
                <div class="alert alert-danger align-content-between">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                <input type="hidden" name="token" required value="{{$token}}">
            <label for="">New password:</label>
            <input type="password" name="password" id="" class="form-control">
            <label for="">Confirm password:</label>
            <input type="password" name="re_password" id="" class="form-control">
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>
@endsection