@extends('base')
@section('content')
<div class="intro">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="section_title text-center">
                    <div>{{__('sentence.Welcome')}}</div>
                    <h1>{{__('sentence.Amazing Hotel in front of the Sea')}}</h1>
                </div>
            </div>
        </div>
        <div class="row intro_row">
            <div class="col-xl-8 col-lg-10 offset-xl-2 offset-lg-1">
                <div class="intro_text text-center">
                    <p>{{__('sentence.With its sleek')}}</p>
                </div>
            </div>
        </div>
        <div class="row gallery_row">
            <div class="col">

                <!-- Gallery -->
                <div class="gallery_slider_container">
                    <div class="owl-carousel owl-theme gallery_slider">

                        <!-- Slide -->
                        <div class="gallery_slide">
                            <img src="images/gallery_1.jpg" alt="">
                            <div class="gallery_overlay">
                                <div class="text-center d-flex flex-column align-items-center justify-content-center">
                                    <a href="#">
                                        <!-- <span>+</span>
                    <span>See More</span> -->
                                    </a>
                                </div>
                            </div>
                        </div>

                        <!-- Slide -->
                        <div class="gallery_slide">
                            <img src="images/gallery_2.jpg" alt="">
                            <div class="gallery_overlay">
                                <div class="text-center d-flex flex-column align-items-center justify-content-center">
                                    <a href="#">
                                        <!-- <span>+</span>
                    <span>See More</span> -->
                                    </a>
                                </div>
                            </div>
                        </div>

                        <!-- Slide -->
                        <div class="gallery_slide">
                            <img src="images/gallery_3.jpg" alt="">
                            <div class="gallery_overlay">
                                <div class="text-center d-flex flex-column align-items-center justify-content-center">
                                    <a href="#">
                                        <!-- <span>+</span>
                    <span>See More</span> -->
                                    </a>
                                </div>
                            </div>
                        </div>

                        <!-- Slide -->
                        <div class="gallery_slide">
                            <img src="images/gallery_4.jpg" alt="">
                            <div class="gallery_overlay">
                                <div class="text-center d-flex flex-column align-items-center justify-content-center">
                                    <a href="#">
                                        <!-- <span>+</span>
                    <span>See More</span> -->
                                    </a>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<!-- Rooms -->

<div class="rooms_right container_wrapper">
    <div class="container">
        <div class="row row-eq-height">

            <!-- Rooms Image -->
            <div class="col-xl-6 order-xl-1 order-2">
                <div class="rooms_slider_container">
                    <div class="owl-carousel owl-theme rooms_slider">

                        <!-- Slide -->
                        <div class="slide">
                            <div class="background_image" style="background-image:url(images/rooms_1.jpg)"></div>
                        </div>

                        <!-- Slide -->
                        <div class="slide">
                            <div class="background_image" style="background-image:url(images/rooms/2019-Premier-Room-03-3.jpg)"></div>
                        </div>

                        <!-- Slide -->
                        <div class="slide">
                            <div class="background_image" style="background-image:url(images/rooms/penthouse-private-dining-at-sunset-picture-id182230218.jpg)"></div>
                        </div>

                    </div>
                </div>
            </div>

            <!-- Rooms Content -->
            <div class="col-xl-6 order-xl-2 order-1">
                <div class="rooms_right_content">
                    <div class="section_title">
                        <div>{{__('sentence.Rooms')}}</div>
                        <h1>{{__('sentence.VIP Room')}}</h1>
                    </div>
                    <div class="rooms_text">
                        <p> {{__('sentence.Executive Guest')}}</p>
                    </div>
                    <div class="rooms_list">
                        <ul>
                            <li class="d-flex flex-row align-items-center justify-content-start">
                                <img src="images/check.png" alt="">
                                <span>{{__('sentence.Room size: 33 m²')}}</span>
                            </li>
                            <li class="d-flex flex-row align-items-center justify-content-start">
                                <img src="images/check.png" alt="">
                                <span>{{__('sentence.Room direction: Beach')}}</span>
                            </li>
                            <li class="d-flex flex-row align-items-center justify-content-start">
                                <img src="images/check.png" alt="">
                                <span>{{__('sentence.Bathroom with shower & bathtub')}}</span>
                            </li>
                        </ul>
                    </div>
                    <div class="rooms_price">$100/<span>{{__('sentence.Night')}}</span></div>

                </div>
            </div>

        </div>
    </div>
</div>

<!-- Rooms -->

<div class="rooms_left container_wrapper">
    <div class="container">
        <div class="row row-eq-height">

            <!-- Rooms Content -->
            <div class="col-xl-6">
                <div class="rooms_left_content">
                    <div class="section_title">
                        <div>{{__('sentence.Rooms')}}</div>
                        <h1>{{__('sentence.Luxury Room')}}</h1>
                    </div>
                    <div class="rooms_text">
                        <p> {{__('sentence.The room')}} </p>
                    </div>
                    <div class="rooms_list">
                        <ul>
                            <li class="d-flex flex-row align-items-center justify-content-start">
                                <img src="images/check.png" alt="">
                                <span>{{__('sentence.Room size: 25 m²')}}</span>
                            </li>
                            <li class="d-flex flex-row align-items-center justify-content-start">
                                <img src="images/check.png" alt="">
                                <span>{{__('sentence.Room direction: Beach')}}</span>
                            </li>
                            <li class="d-flex flex-row align-items-center justify-content-start">
                                <img src="images/check.png" alt="">
                                <span>{{__('sentence.Bathroom with shower & bathtub')}}</span>
                            </li>
                        </ul>
                    </div>
                    <div class="rooms_price">$50/<span>{{__('sentence.Night')}}</span></div>

                </div>
            </div>

            <!-- Rooms Image -->
            <div class="col-xl-6">
                <div class="rooms_slider_container">
                    <div class="owl-carousel owl-theme rooms_slider">

                        <!-- Slide -->
                        <div class="slide">
                            <div class="background_image" style="background-image:url(images/rooms_2.jpg)"></div>
                        </div>

                        <!-- Slide -->
                        <div class="slide">
                            <div class="background_image" style="background-image:url(images/rooms/photo-1568495248636-6432b97bd949.jpg)"></div>
                        </div>

                        <!-- Slide -->
                        <div class="slide">
                            <div class="background_image" style="background-image:url(images/rooms/1578039522_5e0ef8e21e8eb-thumb.jpg)"></div>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

    <!-- Discover Slider -->
    <div class="discover_slider_container">
        <div class="owl-carousel owl-theme discover_slider">

            <!-- Slide -->
            <div class="slide">
                <div class="background_image" style="background-image:url(images/discover_1.jpg)"></div>
                <div class="discover_overlay d-flex flex-column align-items-center justify-content-center">
                    <h1><a href="#">{{__('sentence.Weddings')}}</a></h1>
                </div>
            </div>

            <!-- Slide -->
            <div class="slide">
                <div class="background_image" style="background-image:url(images/discover_2.jpg)"></div>
                <div class="discover_overlay d-flex flex-column align-items-center justify-content-center">
                    <h1><a href="#">{{__('sentence.Parties')}}</a></h1>
                </div>
            </div>

            <!-- Slide -->
            <div class="slide">
                <div class="background_image" style="background-image:url(images/discover_3.jpg)"></div>
                <div class="discover_overlay d-flex flex-column align-items-center justify-content-center">
                    <h1><a href="#">{{__('sentence.Relax')}}</a></h1>
                </div>
            </div>

        </div>
    </div>

</div>

<!-- Testimonials -->

<div class="testimonials">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="section_title text-center">
                    <div>{{__('sentence.Clients')}}</div>
                    <h1>{{__('sentence.Testimonials')}}</h1>
                </div>
            </div>
        </div>
        <div class="row testimonials_row">
            <div class="col">

                <!-- Testimonials Slider -->
                <div class="testimonials_slider_container">
                    <div class="owl-carousel owl-theme testimonials_slider">

                        <!-- Slide -->
                        <div>
                            <div class="testimonial_text text-center">
                                <p>{{__('sentence.We had our')}}</p>
                            </div>
                            <div class="testimonial_author text-center">
                                <div class="testimonial_author_image"><img src="images/trump.jpg" alt=""></div>
                                <div class="testimonial_author_name"><a href="#">Donald Trump,</a><span> President U.S</span>
                                </div>
                            </div>
                        </div>

                        <!-- Slide -->
                        <div>
                            <div class="testimonial_text text-center">
                                <p>{{__('sentence.Thank you')}} </p>
                            </div>
                            <div class="testimonial_author text-center">
                                <div class="testimonial_author_image"><img src="images/mtp.jpg" alt=""></div>
                                <div class="testimonial_author_name"><a href="#">Son Tung M-TP,</a><span> Singer</span>
                                </div>
                            </div>
                        </div>

                        <!-- Slide -->
                        <div>
                            <div class="testimonial_text text-center">
                                <p> {{__('sentence.Miramar Hotel')}} </p>
                            </div>
                            <div class="testimonial_author text-center">
                                <div class="testimonial_author_image"><img src="images/mark.jpg" alt=""></div>
                                <div class="testimonial_author_name"><a href="#">Mark Zuckerberg,</a><span>Cofounder Facebook</span>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
