@extends('base')
@section('content')
  <div class="intro">
    <div class="container">
      <div class="row row-eq-height">

        <!-- Intro Content -->
        <div class="col-lg-6">
          <div class="intro_content">
            <div class="section_title">
              <div>Miramar</div>
              <h1>{{__('sentence.Amazing Hotel in front of the Sea')}}</h1>
            </div>
            <div class="intro_text">
              <p> {{__('sentence.With its sleek')}} </p>
            </div>
           <!--  <div class="button intro_button"><a href="#">read more</a></div> -->
          </div>
        </div>

        <!-- Intro Image -->
        <div class="col-lg-6">
          <div class="intro_image">
            <div class="background_image" style="background-image:url(images/intro.jpg)"></div>
            <img src="images/intro.jpg" alt="">
          </div>
        </div>

      </div>
    </div>
  </div>

  <!-- Offering -->

  <div class="offering">
    <div class="container">
      <div class="row">
        <div class="col">
          <div class="section_title text-center">
            <div> {{__('sentence.Resort')}}</div>
            <h1> {{__('sentence.What we offer')}}</h1>
          </div>
        </div>
      </div>
      <div class="row offering_row">
        
        <!-- Offer Item -->
        <div class="col-xl-4 col-md-6">
          <div class="offer">
            <div class="offer_image"><img src="images/offer_1.jpg" alt=""></div>
            <div class="offer_content text-center">
              <div class="offer_title"><h3> {{__('sentence.Outdoor Pool')}}</h3></div>
              <div class="offer_text">
                <p>{{__('sentence.For many travelers ')}} </p>
              </div>
            <!--   <div class="offer_button"><a href="#">discover</a></div> -->
            </div>
          </div>
        </div>

        <!-- Offer Item -->
        <div class="col-xl-4 col-md-6">
          <div class="offer">
            <div class="offer_image"><img src="images/offer_2.jpg" alt=""></div>
            <div class="offer_content text-center">
              <div class="offer_title"><h3> {{__('sentence.Indoor Pool')}}</h3></div>
              <div class="offer_text">
                <p>{{__('sentence.The facilities are')}} </p>
              </div>
            <!--   <div class="offer_button"><a href="#">discover</a></div> -->
            </div>
          </div>
        </div>

        <!-- Offer Item -->
        <div class="col-xl-4 col-md-6">
          <div class="offer">
            <div class="offer_image"><img src="images/spazone.jpg" alt="" style="height: 265px;"></div>
            <div class="offer_content text-center">
              <div class="offer_title"><h3>{{__('sentence.Spa Zone')}}</h3></div>
              <div class="offer_text">
                <p>{{__('sentence.Luxury hotel')}}</p>
              </div>
            <!--   <div class="offer_button"><a href="#">discover</a></div> -->
            </div>
          </div>
        </div>

        <!-- Offer Item -->
        <div class="col-xl-4 col-md-6">
          <div class="offer">
            <div class="offer_image"><img src="images/sport.jpg" alt="" style="height: 265px;"></div>
            <div class="offer_content text-center">
              <div class="offer_title"><h3>{{__('sentence.Sports Area')}}</h3></div>
              <div class="offer_text">
                <p>{{__('sentence.Inside the')}} </p>
              </div>
            <!--   <div class="offer_button"><a href="#">discover</a></div> -->
            </div>
          </div>
        </div>

        <!-- Offer Item -->
        <div class="col-xl-4 col-md-6">
          <div class="offer">
            <div class="offer_image"><img src="images/offer_5.jpg" alt=""></div>
            <div class="offer_content text-center">
              <div class="offer_title"><h3>{{__('sentence.Restaurant')}}</h3></div>
              <div class="offer_text">
                <p> {{__('sentence.Hotel')}} </p>
              </div>
            <!--   <div class="offer_button"><a href="#">discover</a></div> -->
            </div>
          </div>
        </div>

        <!-- Offer Item -->
        <div class="col-xl-4 col-md-6">
          <div class="offer">
            <div class="offer_image"><img src="images/skybar.jpg" alt="" style="height: 265px;"></div>
            <div class="offer_content text-center">
              <div class="offer_title"><h3>{{__('sentence.Skybar')}}</h3></div>
              <div class="offer_text">
                <p>{{__('sentence.Chilling')}}</p>
              </div>
            <!--   <div class="offer_button"><a href="#">discover</a></div> -->
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>
@endsection
