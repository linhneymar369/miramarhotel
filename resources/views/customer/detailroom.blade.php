@extends('base')
@section('content')
<style>
    .card-inner{
    margin-left: 4rem;
    
}
.content-item {
    padding:30px 0;
	background-color:#FFFFFF;
}

.content-item.grey {
	background-color:#F0F0F0;
	padding:50px 0;
	height:100%;
}

.content-item h2 {
	font-weight:700;
	font-size:35px;
	line-height:45px;
	text-transform:uppercase;
	margin:20px 0;
}

.content-item h3 {
	font-weight:400;
	font-size:20px;
	color:#555555;
	margin:10px 0 15px;
	padding:0;
}

.content-headline {
	height:1px;
	text-align:center;
	margin:20px 0 70px;
}

.content-headline h2 {
	background-color:#FFFFFF;
	display:inline-block;
	margin:-20px auto 0;
	padding:0 20px;
}

.grey .content-headline h2 {
	background-color:#F0F0F0;
}

.content-headline h3 {
	font-size:14px;
	color:#AAAAAA;
	display:block;
}


#comments {
    box-shadow: 0 -1px 6px 1px rgba(0,0,0,0.1);
	background-color:#FFFFFF;
}

#comments form {
	margin-bottom:30px;
}

#comments .btn {
	margin-top:7px;
}

#comments form fieldset {
	clear:both;
}

#comments form textarea {
	height:100px;
}

#comments .media {
	border-top:1px dashed #DDDDDD;
	padding:20px 0;
	margin:0;
}

#comments .media > .pull-left {
    margin-right:20px;
}

#comments .media img {
	max-width:100px;
}

#comments .media h4 {
	margin:0 0 10px;
}

#comments .media h4 span {
	font-size:14px;
	float:right;
	color:#999999;
}

#comments .media p {
	margin-bottom:15px;
	text-align:justify;
}

#comments .media-detail {
	margin:0;
}

#comments .media-detail li {
	color:#AAAAAA;
	font-size:12px;
	padding-right: 10px;
	font-weight:600;
}

#comments .media-detail a:hover {
	text-decoration:underline;
}

#comments .media-detail li:last-child {
	padding-right:0;
}

#comments .media-detail li i {
	color:#666666;
	font-size:15px;
	margin-right:10px;
}

</style>
<div style="margin:50px;" class="section_title text-center">
<div style="color:gray;" >{{__('sentence.Image room')}}</div></div>
<div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
        <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
        <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
        <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner">
        <div class="carousel-item active">
            <img src="{{ asset('images/rooms/'.$detailroom->img1)}}" class="d-block w-100" alt="..." width="1140" height="427">
        
        </div>
        <div class="carousel-item">
            <img src="{{ asset('images/rooms/'.$detailroom->img2)}}" class="d-block w-100" alt="..." width="1140" height="427">
        </div>
        <div class="carousel-item">
            <img src="{{ asset('images/rooms/'.$detailroom->img3)}}" class="d-block w-100" alt="..." width="1140" height="427"> 
        </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>
<div style="margin:100px;" class="section_title text-center">
    <div style="color:gray;">{{__('sentence.Discription')}}</div>
</div>
    <h3>{{$detailroom->description}}</h3>
<div style="margin:100px;" class="section_title text-center">
    <div style="color:gray;">{{__('sentence.Service support')}}</div>
</div>
<table class="table table-striped">
    <thead>
        <tr>
            <th scope="col">{{__('sentence.Name')}}</th>
            <th scope="col">{{__('sentence.Price')}}</th>
        </tr>
    </thead>
    <tbody>
        @foreach($list_service as $service)
        <tr>
            <th scope="row">{{$service->name}}</th>
            <td>{{$service->price}}</td>
        </tr>
        @endforeach
    </tbody>
</table>
<div class="section_title text-center">
    <div style="color:gray; margin:50px;">{{__('sentence.Rates')}}</div>
</div>
<section class="content-item" id="comments">
    <div class="container">   
    	<div class="row">
            <div class="col-sm-12">   
            <form action="postRate/{{$detailroom->id}}" method="POST">
                @csrf
                	<h3 class="pull-left">{{__('sentence.Your Comment')}}</h3>
                	<button type="submit" class="btn btn-normal pull-right">{{__('sentence.Submit')}}</button>
                    <fieldset>
                        <div class="row">
                            <div class="col-sm-3 col-lg-2 hidden-xs">
                            	@if (Auth::check() === true)
                                @if (!empty(Auth::user()->avatar))
                                <img src="{{asset('images/avatar/'. Auth::user()->avatar)}}" alt=""
                                style="height:8rem; border-radius:50%; width: 8rem;">
                                @else
                                <img src="{{asset('images/user.png')}}" alt="" style="height:8rem; border-radius:50%">
                                @endif
                                @else 
                                <img src="{{asset('images/user.png')}}" alt="" style="height:8rem; border-radius:50%">
                                @endif
                            </div>
                            <div class="form-group col-xs-12 col-sm-9 col-lg-10">
                                <textarea class="form-control" id="message" placeholder="Your message" name="content" ></textarea>
                            </div>
                        </div>  	
                    </fieldset>
                </form>
                
                <h3>{{$count}} {{__('sentence.Comments')}}</h3>
                
                <!-- COMMENT - START -->
                @foreach($rates as $rate)
                <div class="media">
                <a class="pull-left" href="#">
                    @if($rate->users->avatar == null)
                    <img class="media-object" src="{{asset('images/user.png')}}" alt="" style="height: 6rem;border-radius:50%;"></a>
                    @else
                    <img class="media-object" src="{{asset('images/avatar/'.$rate->users->avatar)}}" alt="" style="height: 6rem;border-radius:50%;"></a>
                    @endif
                    <div class="media-body">
                        <h4 class="media-heading">{{$rate->users->name}}</h4>
                        <p>{{$rate->content}}</p>
                        <ul class="list-unstyled list-inline media-detail pull-left">
                            <li><i class="fa fa-calendar"></i>{{$rate->created_at}}</li>
                        </ul>
                    </div>
                </div>
                @endforeach
                <!-- COMMENT - END -->
            </div>
        </div>
    </div>
</section>
<div class="button home_button"><a href="">{{__('sentence.Book now')}}</a></div>
@endsection
