@extends('base')
@section('content')
<div class="row">
    <div class="col">
        <div class="card-columns">
            <div>
                @foreach($list_room as $room)
                <form action="{{route('pushToCart',$room->id)}}" method="POST">
                    @csrf
                <div class="card">
                    <img class="card-img-top" src="{{ asset('images/rooms/'.$room->img1)}}" alt="Room image description" width="364"
                        height="242">
                    <div class="card-body">
                        <div class="rooms_title">
                            <h2></h2>
                        </div>
                        <div class="rooms_text">
                            <p></p>
                        </div>
                        <div class="rooms_list">
                            <ul>
                                <li class="d-flex flex-row align-items-center justify-content-start">
                                    <img src="images/check.png" alt="">
                                    <span>{{__('sentence.Number of Person:')}}  {{$room->amount_user}} </span>
                                </li>
                                <li class="d-flex flex-row align-items-center justify-content-start">
                                    <img src="images/check.png" alt="">
                                    <span> {{__('sentence.Type Rooms:')}} {{$room->name}}</span>
                                </li>
                            </ul>
                        </div>
                        <div class="rooms_price">{{$room->price * $room->amount_user}}$/<span>{{__('sentence.Night')}}</span></div>
                        <button type="submit" class="button menu_button" style="width: 35%;cursor: pointer;margin-left: 45px;"><span class="btn-book">{{__('sentence.Book')}} </span></button>
                        <button data-url="" class="button menu_button" style="width: 35%;"><a href="{{route('detailroom', $room->id) }}"> {{__('sentence.Detail')}}</a></button>
                        
                    </div>
                </div>
            </form>
                @endforeach
            </div>
            <div class="d-flex justify-content-end nav">
                @if(!empty($list_room)) {!! $list_room->links('pagination.paginate') !!} @endif
            </div>
        </div>
    </div>
    @endsection
