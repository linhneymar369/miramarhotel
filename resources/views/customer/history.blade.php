@extends('base')
@section('content')
<style>
  td{
    color: black;
    font-weight: bold;
    font-family: system-ui;
  }
</style>
<div class="contact" style="min-height: 700px;">
    <h1 style="text-align: center;font-family: system-ui;padding: 20px;color: darkblue;">{{__('sentence.History booking')}}</h1>

    <div style="border: 0.2rem solid #D7D7D7; padding: 1.5rem;">
    <ul class="nav nav-tabs" id="myTab" role="tablist">
        <li class="nav-item">
          <a class="nav-link active" id="wait-tab" data-toggle="tab" href="#wait" role="tab" aria-controls="wait" aria-selected="true">{{__('sentence.Waitting Confirm')}}</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" id="Confirmed-tab" data-toggle="tab" href="#Confirmed" role="tab" aria-controls="Confirmed" aria-selected="false">{{__('sentence.Confirmed')}}</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" id="Paid-tab" data-toggle="tab" href="#Paid" role="tab" aria-controls="Paid" aria-selected="false">{{__('sentence.Paid')}}</a>
        </li>
      </ul>
      <div class="tab-content" id="myTabContent">
        <div class="tab-pane fade show active" id="wait" role="tabpanel" aria-labelledby="wait-tab">
            <table class="table">
                <thead class="thead-light">
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">{{__('sentence.Type Room')}}</th>
                    <th scope="col">{{__('sentence.Price')}}</th>
                    <th scope="col">{{__('sentence.People')}}</th>
                    <th scope="col">{{__('sentence.Check In')}}</th>
                    <th scope="col">{{__('sentence.Check Out')}}</th>
                    <th scope="col">{{__('sentence.Service')}}</th>
                    <th scope="col">{{__('sentence.Price')}}</th>
                    <th scope="col">{{__('sentence.Total')}}</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach($waits as $wait)
                  <tr>
                    <th scope="row">1</th>
                    <td>{{$wait->rooms->types->name}}</td>
                    <td>{{$wait->rooms->types->price * $wait->rooms->amount_user}}$</td>
                    <td>{{$wait->rooms->amount_user}}</td>
                    <td>{{Carbon\Carbon::parse($wait->check_in)->format('d-m-Y')}}</td>
                    <td>{{Carbon\Carbon::parse($wait->check_out)->format('d-m-Y')}}</td>
                    <td>{{$wait->services->name}}</td>
                    <td>{{$wait->services->price}}$</td>
                    <td>{{$wait->total}}$</td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
        </div>
        <div class="tab-pane fade" id="Confirmed" role="tabpanel" aria-labelledby="Confirmed-tab">
            <table class="table">
                <thead class="thead-light">
                    <thead class="thead-light">
                        <tr>
                          <th scope="col">#</th>
                          <th scope="col">{{__('sentence.Type Room')}}</th>
                          <th scope="col">{{__('sentence.Price')}}</th>
                          <th scope="col">{{__('sentence.People')}}</th>
                          <th scope="col">{{__('sentence.Check In')}}</th>
                          <th scope="col">{{__('sentence.Check Out')}}</th>
                          <th scope="col">{{__('sentence.Service')}}</th>
                          <th scope="col">{{__('sentence.Price')}}</th>
                          <th scope="col">{{__('sentence.Total')}}</th>
                        </tr>
                      </thead>
                      <tbody>
                          @foreach($confirms as $confirm)
                        <tr>
                          <th scope="row">1</th>
                          <td>{{$confirm->rooms->types->name}}</td>
                          <td>{{$confirm->rooms->types->price * $confirm->rooms->amount_user}}$</td>
                          <td>{{$confirm->rooms->amount_user}}</td>
                          <td>{{Carbon\Carbon::parse($confirm->check_in)->format('d-m-Y')}}</td>
                          <td>{{Carbon\Carbon::parse($confirm->check_out)->format('d-m-Y')}}</td>
                          <td>{{$confirm->services->name}}</td>
                          <td>{{$confirm->services->price}}$</td>
                          <td>{{$confirm->total}}$</td>
                        </tr>
                        @endforeach
                      </tbody>
              </table>
        </div>
        <div class="tab-pane fade" id="Paid" role="tabpanel" aria-labelledby="Paid-tab">
            <table class="table">
                <thead class="thead-light">
                    <tr>
                      <th scope="col">#</th>
                      <th scope="col">{{__('sentence.Type Room')}}</th>
                      <th scope="col">{{__('sentence.Price')}}</th>
                      <th scope="col">{{__('sentence.People')}}</th>
                      <th scope="col">{{__('sentence.Check In')}}</th>
                      <th scope="col">{{__('sentence.Check Out')}}</th>
                      <th scope="col">{{__('sentence.Service')}}</th>
                      <th scope="col">{{__('sentence.Price')}}</th>
                      <th scope="col">{{__('sentence.Total')}}</th>
                    </tr>
                  </thead>
                  <tbody>
                      @foreach($paids as $paid)
                    <tr>
                      <th scope="row">1</th>
                      <td>{{$paid->rooms->types->name}}</td>
                      <td>{{$paid->rooms->types->price * $paid->rooms->amount_user}}$</td>
                      <td>{{$paid->rooms->amount_user}}</td>
                      <td>{{Carbon\Carbon::parse($paid->check_in)->format('d-m-Y')}}</td>
                      <td>{{Carbon\Carbon::parse($paid->check_out)->format('d-m-Y')}}</td>
                      <td>{{$paid->services->name}}</td>
                      <td>{{$paid->services->price}}$</td>
                      <td>{{$paid->total}}$</td>
                    </tr>
                    @endforeach
                  </tbody>
              </table>
        </div>
      </div>
    </div>
</div>
@endsection