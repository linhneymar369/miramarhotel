<!-- Contact -->
@extends('base')
@section('content')
  <div class="contact" style="min-height: 700px;">
    <div class="contact_container">
      <div class="container">
        <div class="row">
          <div class="col">
            <div class="section_title text-center">
              <div>{{__('sentence.Contact')}}</div>
              <h1>{{__('sentence.Say Hello')}}</h1>
            </div>
            <div class="contact_text text-center">
              <p>{{__('sentence.Ready')}}Ready</p>
            </div>
        
            <div class="contact_form_container">
              <form action="{{route('message.store')}}" method="POST" id="sendmail-form" class="contact_form text-center">
                @csrf
                <div class="col">
                  <div class="row-lg-6 input__contact">
                    <input type="text" class="form-control" name="name" placeholder="{{__('sentence.Your name')}}" maxlength="100">
                  </div>
                  <div class="row-lg-6 input__contact">
                    <input type="email" class="form-control" name="mail" placeholder="{{__('sentence.Your email')}}" maxlength="100">
                  </div>
                  <div class="row-lg-6 input__contact">
                    <input type="text" class="form-control" name="subject" placeholder="{{__('sentence.Subject')}}" maxlength="100">
                  </div>
                  <div class="input__contact">
                    <textarea class="form-control " name="message" placeholder="{{__('sentence.Message')}}" maxlength="5000"></textarea>
                  </div>
                </div>
                <button class="btn btn-success input__contact">{{__('sentence.Send message')}}</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
  <script>
  $("#sendmail-form").validate({
    rules: {
        name: {
            required: true,
            minlength: 10,
            maxlength: 100
        },
        email: {
            required: true,
            minlength: 10,
            email: true,
            maxlength: 100
        },
        subject: {
            required: true,
            maxlength: 100
        },
        message: {
            required: true,
            maxlength: 5000
        }
    },
    errorElement: 'span',
    errorPlacement: function(error, element) {
        error.addClass('has-error').css("color", "red");
        element.closest('.input__contact').append(error);
    },
    highlight: function(element, errorClass, validClass) {
        $(element).addClass('is-invalid');
    },
    unhighlight: function(element, errorClass, validClass) {
        $(element).removeClass('is-invalid');
    }
});
  </script>
@endsection
