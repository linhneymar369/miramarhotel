@extends('base')
@section('content')

    <div id="booking">
        <h1 style="text-align: center;margin:50px;">{{__('sentence.yourbookingcart')}}</h1>

        <table class="table">
            <thead class="thead-dark">
                <tr>
                    <th scope="col"></th>
                    <th scope="col">{{__('sentence.Rooms')}}</th>
                    <th scope="col">{{__('sentence.People')}}</th>
                    <th scope="col">{{__('sentence.Price')}}</th>
                    <th scope="col">{{__('sentence.Check In')}}</th>
                    <th scope="col">{{__('sentence.Check Out')}}</th>
                    <th scope="col">{{__('sentence.Service')}}</th>
                    <th scope="col">{{__('sentence.Price')}}</th>
                    <th scope="col">{{__('sentence.Total')}}</th>
                    <th scope="col">{{__('sentence.Action')}}</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($carts as $cart)
                <form action="{{route('sendBookingToAdmin',$cart->id)}}" method="post">
                    @method('put')
                    @csrf
                    <tr>
                        <td><a href="#" class="fa fa-times fa-2x" aria-hidden="true" data-toggle="modal" data-target="#exampleModal"
                            onClick="onClickDelete({{$cart->id}})" style="color:red;"></a></td>
                        <th scope="row">{{ $cart->rooms->types->name }}</th>
                        <td id="people">{{ $cart->rooms->amount_user }}</td>
                        <td id="price" class="priceRoom">{{ $cart->rooms->types->price*$cart->rooms->amount_user }}</td>
                        <td class="checkIn"><input name="check_in" type="date" id="checkin" class="checkin" style="border-color: lavenderblush;"
                                value="{{ Carbon\Carbon::parse($cart->check_in)->format('Y-m-d') }}"></td>
                        <td class="checkOut"><input name="check_out" type="date" id="checkout" class="checkout" style="border-color: lavenderblush;"
                                value="{{ Carbon\Carbon::parse($cart->check_out)->format('Y-m-d') }}"></td>
                        <td>
                            <select name="service" class="service" style="border-color: lavenderblush;border-radius: 4px;padding: 3px;">
                                @foreach ($services as $service)
                                    <option value="{{$service->id}}">{{ $service->name }}</option>
                                @endforeach
                            </select>
                        </td>
                        <td id="service" class="priceService">0</td>
                        <td class="total"><input type="text" name="total" id="total" style="border: none; width: 60px;"
                                value="{{ $cart->rooms->types->price * $cart->rooms->amount_user }}"></td>
                        <td><button type="submit" class="btn btn-success" style="max-height: 28px;text-align: center;line-height: 14px;">{{__('sentence.Book')}}</button></td>
                    </tr>
                </form>
                @endforeach
            </tbody>
        </table>
    </div>
    <form style="display:inline" action="" method="post" id="delete-form">
        @csrf
        @method('DELETE')
        <div class="modal" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content" style="border-radius: 1rem;">
                    <div class="modal-header" style="background-color: #2A3F54;">
                        <h5 class="modal-title" id="exampleModalLabel" style="color:white;">{{__('sentence.Delete')}}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <h1 style="text-align:center;">{{__('sentence.Are you sure')}}</h1>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal"
                            style="width: 100px;">{{__('sentence.Close')}}</button>
                        <button type="submit" class="btn btn-danger" data-toggle="modal" data-target="#successdelete"
                            onClick="submitdelete()" style="width: 67px;">{{__('sentence.Yes')}}</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <script>
        var id = price.value;
        var arr = document.getElementsByClassName("service");
        Array.from(arr).forEach((each) => {
            each.addEventListener("change", function() {
                $.ajax({
                    type: 'GET',
                    url: 'cart/' + this.value,
                    dataType: 'json',
                    error: function() {
                        console.log(data);
                    }
                }).done(function(data) {
                    $(each).parent().next().text(data.price);
                    var checkout = new Date($(each).parent().prev().children().val());
                    var checkin =  new Date($(each).parent().prev().prev().children().val());
                    var diff = checkout.getTime() - checkin.getTime();
                    var night = diff / (24 * 60 * 60 * 1000);
                    var price = parseInt($(each).parent().prev().prev().prev().text());
                    var priceService = parseInt($(each).parent().next().text());
                    var total = (price*night) + priceService;
                    $(each).parent().next().next().children().val(total);
                });
            });
        })
        var arr2 = document.getElementsByClassName('checkout');
        Array.from(arr2).forEach((each) => {
        each.min = $(each).parent().prev().children().val();
        each.addEventListener("change",function() {
        $(each).parent().prev().children().attr("max",$(each).val());
        var checkout = new Date($(each).val());
        var checkin =  new Date($(each).parent().siblings('.checkIn').children().val());
        var diff = checkout.getTime() - checkin.getTime();
        var night = diff / (24 * 60 * 60 * 1000);
        var price = parseInt($(each).parent().siblings('.priceRoom').text());
        var priceService = parseInt($(each).parent().next().next().text());
        var total = (price*night) + priceService;
        $(each).parent().next().next().next().children().val(total);
        })
    })
        var arr3 = document.getElementsByClassName('checkin');
        Array.from(arr3).forEach((each) => {
        each.min = each.value;
        each.max = $(each).parent().next().children().val();
        each.addEventListener("change",function() {
        $(each).parent().next().children().attr("min",$(each).val());
        var checkout = new Date($(each).parent().next().children().val());
        var checkin =  new Date($(each).val());
        var diff = checkout.getTime() - checkin.getTime();
        var night = diff / (24 * 60 * 60 * 1000);
        var price = parseInt($(each).parent().prev().text());
        var priceService = parseInt($(each).parent().next().next().next().text());
        var total = (price*night) + priceService;
        $(each).parent().next().next().next().next().children().val(total); 
        })
    }) 
    const onClickDelete = (id) => {
        const deleteForm = document.getElementById('delete-form');
        deleteForm.action = '/cart/' + id;
    };
    
    function submitdelete() {
        const deleteForm = document.getElementById('delete-form');
        deleteForm.submit();
    }
    </script>
@endsection
