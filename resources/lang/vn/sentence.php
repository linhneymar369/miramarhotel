<?php

//english
return array(
  'home' => 'Trang Chủ',
  'aboutus' => 'Về chúng tôi',
  'rooms' => 'Phòng',
  'contact' => 'Liên hệ',
  'yourbookingcart' => 'Giỏ hàng của bạn',
  'booknow' => 'Đặt ngay',
  'bookyourstay' => 'Đặt phòng của bạn',
  'bookyourstaydown' => 'Miramar Hotel & Resort là một sự lựa chọn hoàn hảo cho dù bạn đi công tác hay nghỉ dưỡng. Đội ngũ nhân viên chuyên nghiệp và niềm nở của chúng tôi rất mong được chào đón bạn.',
  'login' => 'Đăng nhập',
  'signup' => 'Đăng ký',
  'logout' => 'Đăng xuất',
  'profile' => 'Hồ sơ',
  'history' => 'Lịch sử đặt phòng',
  'forgetpassword' => 'Quên mật khẩu',
  'email' => 'Email',
  'password' => 'Mật khẩu',
  'rememberme' => 'Ghi nhớ',
  'enteryouremail' => 'Nhập email của bạn',
  'fullname' => 'Tên đầy đủ',
  'enteryourfullname' => 'Nhập tên đầy đủ của bạn',
  'enteryourpassword' => 'Nhập mật khẩu của bạn',
  'confirmpassword' => 'Xác nhận mật khẩu',
  'reenteryourpassword' => 'Nhập lại mật khẩu',
  'Welcome' => 'Chào mừng',
  'Amazing Hotel in front of the Sea' => 'Khách Sạn Tuyệt Vời Trước Biển',
  'With its sleek' => 'Với mặt tiền bóng bẩy và sức hấp dẫn cao tầng,
  Setai giống một khách sạn đô thị sành điệu hơn là một khu nghỉ mát bên bờ biển -
  một sự tương phản mới mẻ so với những người hàng xóm South Beach sặc sỡ, màu kẹo.
  Bên trong có thể tìm thấy nét thẩm mỹ nổi bật của Art Deco-meeting-Oriental: đá cẩm thạch màu đá phiến,
  điểm nhấn bằng gỗ tếch và tác phẩm nghệ thuật châu Á. Các suite tương tự như zen và có đồ thủ công rất lớn
  Giường gỗ kiểu Thụy Điển và bồn ngâm bằng đá granit đen xa hoa. Bên ngoài là ba hồ bơi tuyệt đẹp -
  được làm nóng ở 75 độ F, 85 độ F và 90 độ tương ứng -
  cách South Beach vài bước chân',
  'Rooms' => 'Phòng',
  'VIP Room' => 'Phòng Vip',
  'Executive Guest' => 'Phòng khách Executive được trang bị tinh xảo và đã được thiết kế
  để pha trộn giữa lối trang trí đầy phong cách của màu sắc trơn và đồ nội thất xa hoa bằng gỗ
  bàn giám đốc và một bộ sofa với những tiện ích cần thiết. Nằm trên 8,9
  tầng nhìn ra quang cảnh tuyệt đẹp của thành phố Phnom Penh và ánh sáng ban đêm.
  Phòng được trang trí sang trọng này có các kênh truyền hình vệ tinh màn hình phẳng, minibar,
  tủ quần áo và két sắt. Phòng tắm với vòi hoa sen hoặc bồn tắm được thiết kế theo phong cách khác biệt và hiện đại.',
  'Room size: 33 m²' => 'Diện tích phòng: 33 m²',
  'Room direction: Beach' => 'Hướng phòng: Bãi biển',
  'Bathroom with shower & bathtub' => 'Phòng tắm vòi sen & bồn tắm',
  'Rooms' => 'Phòng',
  'Luxury Room' => 'Phòng Sang Trọng',
  'The room' => 'Căn phòng có diện tích trên 25m2, bao gồm các thiết bị và đồ dùng hiện đại, sang trọng, đầy đủ tiện nghi cao cấp mang đến cho du khách. 
  Đây là loại phòng đẹp và sang trọng nhất của khách sạn với nội thất đẹp, giường lớn, cửa sổ lớn thoáng mát nhìn ra thành phố. 
  Đặc biệt, phòng khách riêng được thiết kế hiện đại với những chiếc ghế sofa êm ái tạo cho bạn cảm giác thoải mái khi nghỉ ngơi và làm việc.',
  'Room size: 25 m²' => 'Diện tích phòng: 25 m²',
  'Room direction: Beach' => 'Hướng phòng: Bãi biển',
  'Bathroom with shower & bathtub' => 'Phòng tắm vòi sen & bồn tắm',
  'Weddings'  => 'Đám Cưới',
  'Parties' => 'Tiệc Tùng',
  'Relax' => 'Thư Giãn',
  'Clients' => 'Khách hàng',
  'We had our' => 'Chúng tôi đã có cơ sở ngoại vi hàng năm của công ty chúng tôi tại Miramar Hotel & Resort. Miramar là sự lựa chọn hoàn hảo cho chúng tôi:
  có nhiều không gian sự kiện lớn cho các hoạt động khác nhau và đội ngũ nhân viên tuyệt vời đã đảm bảo
  các sự kiện diễn ra suôn sẻ. Thức ăn của khách sạn cũng có chất lượng rất cao với
  sự lựa chọn ấn tượng vào bữa sáng. Kết hợp với tầm nhìn tuyệt đẹp ra biển và bãi biển riêng,
  chúng tôi không thể yêu cầu một nơi tốt hơn cho công ty của chúng tôi. Cảm ơn bạn đã làm cho chuyến đi của chúng tôi rất đặc biệt.',
  'Testimonials' => 'Lời chứng thực',
  'Thank you' => 'Xin gửi lời cảm ơn chân thành đến đội ngũ Miramar Hotel & Resort. Tôi đã để lại thứ gì đó ở khách sạn và cũng muốn chúng
  để phát hành một biên lai khác cho mục đích thuế. Họ ngay lập tức làm việc với nó và biên nhận đã được gửi đến
  giờ qua email. Hàng đã về ngay trong ngày. Hoàn toàn ngoài mong đợi của tôi. Bạn hoàn toàn có thể
  tin tưởng khách sạn này! Trong khi ở đó mọi thứ rất sạch sẽ và dịch vụ rất chu đáo.
  Sẽ luôn luôn là một khách hàng ở đây.',
  'Miramar Hotel' => 'Miramar Hotel & Resort đã chăm sóc chúng tôi tốt hơn bất kỳ nơi nào khác mà chúng tôi từng đến.
  Đây là trải nghiệm quốc tế tốt nhất mà chúng tôi có được trong nhiều năm. Nhân viên của bạn thật tuyệt vời,
  bất cứ điều gì chúng tôi cần họ mang đến cho chúng tôi. Họ hoạt động hiệu quả, chu đáo và không xâm phạm.
  Tôi cũng rất ấn tượng với trang thiết bị, phòng ốc tiện nghi và đồ ăn.
  Cảm ơn bạn rất nhiều vì đã làm cho hội nghị này rất thành công. Tôi thực sự giới thiệu Miramar Hotel & Resort cho bất kỳ sự kiện nào',

  'Standard Swimming Pool' => 'bể bơi đạt chuẩn',
  'Amazing Hotel in front of the Sea' => 'Khách Sạn Tuyệt Vời Trước Biển',
  'With its sleek' => 'Với mặt tiền kiểu dáng đẹp và sự hấp dẫn của những tòa nhà cao tầng, The Setai có cảm giác giống như một khách sạn đô thị sành điệu hơn là một khu nghỉ mát bên bờ biển - một sự tương phản mới mẻ so với những người hàng xóm South Beach màu kẹo sặc sỡ.
  Bên trong có thể tìm thấy nét thẩm mỹ theo phong cách Art Deco đáp ứng-phương Đông: đá cẩm thạch màu đá phiến, các điểm nhấn bằng gỗ tếch và các tác phẩm nghệ thuật châu Á.
  Các suite cũng tương tự như zen và có giường lớn bằng gỗ thủ công của Thụy Điển và bồn ngâm bằng đá granit đen sang trọng.
  Bên ngoài là ba hồ bơi tuyệt đẹp - được làm nóng lần lượt ở 75 độ F, 85 độ và 90 độ - nằm cách South Beach vài bước chân.',
  'Resort' => 'Chỗ nghĩ dưỡng',
  'What we offer' => 'Những gì chúng tôi cung cấp',
  'Outdoor Pool' => 'Bể Bơi Ngoài Trời',
  'For many travelers ' => 'Đối với nhiều du khách, bể bơi vô cực là hình ảnh thu nhỏ của sự sang trọng.
  Có điều gì đó về việc nổi gần với bờ vực gợi lên cảm xúc
  tự do và vô tư - và làm nền rực rỡ cho những khoảnh khắc kỳ nghỉ của bạn.
  Và tại Miramar Hotel & Resort, bạn thậm chí không cần phải chia sẻ hồ bơi vô cực của mình với bất kỳ ai khác,
  vì nó đến như một phần của khu bảo tồn riêng tư của bạn.',
  'Indoor Pool' => 'Hồ Bơi Trong Nhà',
  'The facilities are' => 'Các thiết bị được trang bị đầy đủ và đa dạng trong hồ bơi như ghế nằm,
  bàn ghế, khăn tắm, nên bể bơi của Miramar Hotel & Resort là hoàn hảo nhất
  sự lựa chọn cho du khách khi đến với khách sạn.',
  'Spa Zone' => 'Khu Spa',
  'Luxury hotel' => 'Khách sạn sang trọng với khu spa tuyệt vời. Sự lựa chọn thực phẩm tuyệt vời,
  phòng đẹp và nhân viên hữu ích. Một điều trị đặc biệt là vào mùa đông trong khi
   tuyết rơi bên ngoài và chúng tôi tắm trong một hồ bơi khoáng nóng. Một công viên nước tuyệt vời,
   mà cũng có sẵn cho khách của khách sạn vào mùa hè.',
  'Sports Area' => 'Khu Thể Thao',
  'Inside the' => 'Bên trong Miramar Hotel & Resort, chúng tôi trang bị một khu thể thao cho những người lưu trú dài ngày,
   Trang thiết bị thiết bị hiện đại và đạt tiêu chuẩn 5 *. Khu thể thao làm việc 24/7 luôn chào đón khách hàng
   với đội ngũ PT được đào tạo chuyên nghiệp.',
  'Restaurant' => 'Nhà Hàng',
  'Hotel' => 'Nhà hàng khách sạn là nhà hàng đầu tiên và quan trọng nhất.
   Họ cung cấp dịch vụ tương tự như các đối tác tự trị của họ:
   nhà bếp, đồ ăn, phòng tiếp khách và nhân viên. Chúng tôi đã nói chuyện với các chuyên gia,
   quản lý khách sạn và giám sát nhà hàng cho các nhóm khách sạn để tốt hơn
   hiểu các vấn đề của thương mại và các biện pháp được thực hiện để ứng phó với chúng.',
  'Skybar' => 'Skybar',
  'Chilling' => 'Bầu không khí lạnh với đồ uống ngon và dịch vụ tốt. Uống rượu và tận hưởng cảm giác như đang ở trên không.
   Tất nhiên, đó là cách tốt hơn để tận hưởng cả đêm với một cặp đôi ly rượu vang đỏ.',

  'Contact' => 'Liên hệ',
  'Say Hello' => 'Xin Chào',
  'Ready' => 'Sẵn sàng tham quan? Đội ngũ nhân viên thân thiện, hiểu biết của chúng tôi sẵn sàng giúp bạn lên kế hoạch cho kỳ nghỉ của mình. 
   Gọi cho họ hoặc điền vào biểu mẫu bên dưới chúng tôi xuất sắc trong việc dự đoán nhu cầu của bạn và sẽ đưa ra các đề xuất để giúp bạn tận dụng tối đa kỳ nghỉ của mình.',
  'Send message' => 'Gởi Tin Nhắn',

  'Number of Person:' => 'Số Người :',
  'Type Rooms:' => 'Loại Phòng:',
  'Book' => 'Đặt',
  'Detail' => 'Chi Tiết',

  'Image room' => 'Hình Ảnh Phòng',
  'Discription' => 'Mô Tả',
  'Service support' => 'Hỗ Trợ Dịch Vụ',
  'Name' => 'Tên',
  'Price' => 'Giá',
  'Rates' => 'Đánh Giá',
  'Your Comment' => 'Bình Luận Của Bạn',
  'Submit' => 'Gởi',
  'Comments' => 'Bình Luận',
  'Book now' => 'Đặt Bây Giờ',
  'Type Room' => 'Kiểu phòng',
  'Your Booking Cart' => 'Giỏ hàng đặt trước của bạn',
  'Room' => 'Phòng',
  'Check In' => 'Ngày Vào',
  'Check Out' => 'Ngày Ra',
  'Night' => 'Đêm',
  'People' => 'Người',
  'Action' => 'Hành Động',
  'Total' => 'Tổng',
  'Price' => 'Giá',
  'Service' => 'Dịch vụ',
  'Your name' => 'Tên Của Bạn',
  'Your email' => 'Email Của Bạn',
  'Subject' => 'Vấn Đề',
  'Message' => 'Thông Điệp',
  'Edit profile' => 'Chỉnh sửa hồ sơ',
  'Change Password' => 'Đổi mật khẩu',
  'User Profile' => 'Hồ sơ tài khoản',
  'Address' => 'Địa chỉ',
  'Phone' => 'Điện thoại',
  'Gender' => 'Giới tính',
  'Birth day' => 'Sinh nhật',
  'Identify Card' => 'Số CMND',
  'Save Changes' => 'Lưu thay đổi',
  'Old Password' => 'Mật khẩu cũ',
  'New Password' => 'Mật khẩu mới',
  'Confirm New Password' => 'Xác nhận mật khẩu mới',
  'Upload your avatar' => 'Tài lên ảnh đại diện',
  'Choose file' => 'Chọn tệp',
  'Save Avatar' => 'Lưu ảnh đại diện',
  'Null' => 'Trống',
  'Delete' => 'Xóa',
  'Are you sure' => 'Bạn chắc chứ?',
  'Close' => 'Đóng',
  'Yes' => 'Có',
  'Edit your profile successfully' => 'Chỉnh sửa hồ sơ của bạn thành công!',
  'Change avatar successfully' => 'Đổi ảnh đại diện thành công!',
  'Change password successfully' => 'Đổi mật khẩu thành công!',
  'Your password not correct,please try again' => 'Mật khẩu của bạn không chính xác, vui lòng thử lại!',
  'Confirm new password not same' => 'Xác nhận mật khẩu không giống nhau',
  'There are errors please contact the admin website' => 'Có lỗi xảy ra, vui lòng liên hệ quản trị trang!',
  'Please check info after booking' => 'Vui lòng kiểm tra lại thông tin trước khi đặt!',
  'Your booking has been send to Admin, please wait confirm' => 'Đặt phòng của bạn đã được gửi tới quản trị, vui lòng đợi xác nhận!',
  'This room has been booked, please change room or change day stay' => 'Phòng này đã được đặt, vui lòng đổi phòng hoặc đổi ngày ở!',
  'History booking' => 'Lịch sử đặt phòng',
  'Waitting Confirm' => 'Đang đợi xác nhận',
  'Confirmed' => 'Đã xác nhận',
  'Paid' => 'Đã thanh toán',
  'Invalid Email or Password' => 'Email hoặc mật khẩu không hợp lệ!',
  'Your account looked, please contact admin' => 'Tài khoản của bạn đã bị khóa, vui lòng liên hệ quản trị!',
  'Login Successfully' => 'Đăng nhập thành công!',
  'Your email already exist in system, please try again' => 'Email của bạn đã tồn tại trong hệ thống, vui lòng thử lại!',
  'Confirm password not same' => 'Xác nhận mật khẩu không giống nhau!',
  'Register Account Successfully' => 'Đăng ký tài khoản thành công!',
  'Email not found please try again' => 'Email không thể tìm thấy, vui lòng thử lại!',
  'A request change password has been sent to your email, maybe the email is in the spam box, please check' => 'Yêu cầu đổi mật khẩu đã được gửi đến email của bạn, có thể email nằm trong thư rác, vui lòng kiểm tra!',
  'Please Sign In or Sign Up' => 'Vui lòng đăng nhập hoặc đăng ký!',
  'Wrong your account, please contact admin' => 'Có vấn đề với tài khoản của bạn, vui lòng liên hệ quản trị!',
);
