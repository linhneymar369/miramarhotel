<?php

//english

return array(
  'home' => 'Home',
  'aboutus' => 'About Us',
  'rooms' => 'Rooms',
  'contact' => 'Contact',
  'yourbookingcart' => 'Your Booking Cart',
  'booknow' => 'Book Now',
  'bookyourstay' => 'Book Your Stay',
  'bookyourstaydown' => 'Miramar Hotel & Resort  is a perfect choice whether you are on business trip or vacation. Our professional and warm staff are looking forward to welcoming you.',
  'login' => 'Login',
  'signup' => 'Sign Up',
  'logout' => 'Logout',
  'profile' => 'Profile',
  'history' => 'History Booking',
  'forgetpassword' => 'Forget Password',
  'email' => 'Email',
  'password' => 'Password',
  'rememberme' => 'Remember me',
  'enteryouremail' => 'Enter Your Email',
  'fullname' => 'Full name',
  'enteryourfullname' => 'Enter Your Full Name',
  'enteryourpassword' => 'Enter Your Password',
  'confirmpassword' => 'Confirm Password',
  'reenteryourpassword' => 'Reenter Your Password',
  'Welcome' => 'Welcome',
  'Amazing Hotel in front of the Sea' => 'Amazing Hotel In Front Of The Sea',
  'With its sleek' => 'With its sleek facade and high-rise appeal, 
  The Setai feels more like a hip urban hotel than a beachfront resort --
  a refreshing contrast from its garish, candy-colored South Beach neighbors. 
  Inside can be found a striking Art Deco-meets-Eastern aesthetic: slate-colored marble,
  teak accents and Asian artwork. Suites are similarly zen and feature enormous handmade 
  Swedish wooden beds and sumptuous black granite soaking tubs. Outside are three stunning pools -- 
  heated at 75 degrees Fahrenheit, 85 degrees and 90 degrees respectively -- 
  located steps from South Beach',
  'Rooms' => 'Rooms',
  'VIP Room' => 'VIP ROOM',
  'Executive Guest' => 'Executive Guest Rooms are exquisitely appointed and have been designed 
  to blend stylish decor of smooth colors and sumptuous furnishings featuring wooden 
  executive desk and a set of sofa with the essential convenience. Located on the 8,9 
  floors overlooking the stunning views of the Phnom Penh’s city and night light. 
  This luxuriously decorated room features a flat-screen satellite channels, minibar, 
  wardrobe and safe box. The bathroom with shower or a bath tub design difference style and modern.',
  'Room size: 33 m²' => 'Room size: 33 m²',
  'Room direction: Beach' => 'Room direction: Beach',
  'Bathroom with shower & bathtub' => 'Bathroom with shower & bathtub',
  'Rooms' => 'Rooms',
  'Luxury Room' => 'Luxury Room',
  'The room' => 'The room has an area of ​​over 25m2, including equipment and modern appliances,
  luxury, fully equipped luxury offered to guests. This is kind of the most beautiful
  and luxurious rooms of the hotel with beautiful furniture, large beds, large airy windows
  overlooking the city. In particular, a separate living room has a modern design with smooth 
  sofas make you feel comfortable at rest and work.',
  'Room size: 25 m²' => 'Room size: 25 m²',
  'Room direction: Beach' => 'Room direction: Beach',
  'Bathroom with shower & bathtub' => 'Bathroom with shower & bathtub',
  'Weddings'  => 'Weddings',
  'Parties' => 'Parties',
  'Relax' => 'Relax',
  'Clients' => 'Clients',
  'We had our' => 'We had our company s annual offsite at the Miramar Hotel & Resort. Miramar was the perfect choice for us: 
    there are multiple large event spaces for various activities and the great staff made sure the 
    events went smoothly. The hotel food was also of very high quality with an 
    impressive choice at breakfast. Combined with a stunning view of the sea and a private beach, 
    we couldn’t have asked for a better place for our company. Thank you for making our trip so special.',
  'Testimonials' => 'Testimonials',
  'Thank you' => 'Thank you so much to the Miramar Hotel & Resort team. I left something behind at the hotel and also wanted them 
    to issue another receipt for tax purposes. They immediately worked on it and the receipt was sent in 
    the hour by email. The item was returned within the day. Totally beyond my expectation. You can totally 
    trust this hotel! Whilst staying there everything was so clean and the service was very attentive. 
    Will always be a customer here. ',
  'Miramar Hotel' => 'Miramar Hotel & Resort took care of us better than any other place we have been to. 
    This was the best international experience we have had in years. Your staff was amazing, 
    anything we needed they brought to us. They were efficient, attentive, and not intrusive.
    I was also very impressed with the equipment, the comfortable rooms, and food. Thank you very much for making this conference so successful. I highly recommend the Miramar Hotel & Resort for any event',
  'Amazing Hotel in front of the Sea' => 'Amazing Hotel In Front Of The Sea',
  'With its sleek' => 'With its sleek facade and high-rise appeal, The Setai feels more like a hip urban hotel than a beachfront resort -- a refreshing contrast from its garish, candy-colored South Beach neighbors.
    Inside can be found a striking Art Deco-meets-Eastern aesthetic: slate-colored marble, teak accents and Asian artwork.
    Suites are similarly zen and feature enormous handmade Swedish wooden beds and sumptuous black granite soaking tubs.
    Outside are three stunning pools -- heated at 75 degrees Fahrenheit, 85 degrees and 90 degrees respectively -- located steps from South Beach.',
  'Resort' => 'Resort',
  'What we offer' => 'What we offer',
  'Outdoor Pool' => 'Outdoor Pool',
  'For many travelers ' => 'For many travelers, infinity pools are the epitome of luxury. 
    There’s something about floating close to the edge that evokes feelings 
    of freedom and carefreeness – and makes a brilliant background for your holiday snaps. 
    And at Miramar Hotel & Resort, you won’t even have to share your infinity pool with anyone else, 
    since it comes as part of your own private sanctuary.',
  'Indoor Pool' => 'Indoor Pool',
  'The facilities are' => 'The facilities are fully equipped and diverse in the pool such as loungers,
    tables and chairs, towels, and so the swimming pool of Miramar Hotel & Resort is the perfect 
    choice for travelers when coming to the hotel.',
  'Spa Zone' => 'Spa Zone',
  'Luxury hotel' => 'Luxury hotel with a fabulous spa area. The choice of food excellent,
    beautiful rooms and helpful staff. A special treat was in the winter while
     snowing outside and us bathing in a hot mineral pool. A great aqua park, 
     which is also available to hotel guests in the summer.',
  'Sports Area' => 'Sports Area',
  'Inside the' => 'Inside the Miramar Hotel & Resort we equipment a sports area for long-stay peoples, 
     equipment devices modern and standard 5*. Sports Area working 24/7 always welcome customer 
     with PT team professionally trained.',
  'Restaurant' => 'Restaurant',
  'Hotel' => 'Hotel restaurants are first and foremost restaurants.
     They offer the same service as their autonomous counterparts: 
     a kitchen, food, a seating room, and a staff. We talked to experts, 
     hotel managers, and restaurant supervisors for hotel groups to better 
     understand the issues of the trade and the measures taken to respond to them.',
  'Skybar' => 'Skybar',
  'Chilling' => 'Chilling atmosphere with nice drinks and good service. Drink wine and enjoy feel like being up in the air. 
     Of course its the way better to enjoy the whole night with a couples glasses of red wine.',

  'Contact' => 'Contact',
  'Say Hello' => 'Say Hello',
  'Ready' => 'Ready to visit? Our friendly, knowledgeable staff is ready to help you plan your vacation. Give them a call or fill out the form below — we excel at anticipating your needs, 
     and will offer suggestions to help you get the most out of your stay.',
  'Send message' => 'Send message',

  'Number of Person:' => "Number of Person:",
  'Type Rooms:' => 'Type Rooms:',
  'Book' => 'Book',
  'Detail' => 'Detail',

  'Image room' => 'Image room',
  'Discription' => 'Discription',
  'Service support' => 'Service Support',
  'Name' => 'Name',
  'Price' => 'Price',
  'Rates' => 'Rates',
  'Your Comment' => 'Your Comment',
  'Submit' => 'Submit',
  'Comments' => 'Comments',
  'Book now' => 'Book now',
  'Type Room' => 'Type Room',
  'Your Booking Cart' => 'Your Booking Cart',
  'Room' => 'Room',
  'Check In' => 'Check In',
  'Check Out' => 'Check Out',
  'Night' => 'Night',
  'People' => 'People',
  'Action' => 'Action',
  'Total' => 'Total',
  'Price' => 'Price',
  'Service' => 'Service',
  'Your name' => 'Your name',
  'Your email' => 'Your email',
  'Subject' => 'Subject',
  'Message' => 'Message',
  'Edit profile' => 'Edit profile',
  'Change Password' => 'Change Password',
  'User Profile' => 'User Profile',
  'Address' => 'Address',
  'Phone' => 'Phone',
  'Gender' => 'Gender',
  'Birth day' => 'Birth day',
  'Identify Card' => 'Identify Card',
  'Save Changes' => 'Save Changes',
  'Old Password' => 'Old Password',
  'New Password' => 'New Password',
  'Confirm New Password' => 'Confirm New Password',
  'Upload your avatar' => 'Upload your avatar',
  'Choose file' => 'Choose file',
  'Save Avatar' => 'Save Avatar',
  'Null' => 'Null',
  'Delete' => 'Delete',
  'Are you sure' => 'Are you sure?',
  'Close' => 'Close',
  'Yes' => 'Yes',
  'Edit your profile successfully' => 'Edit your profile successfully!',
  'Change avatar successfully' => 'Change avatar successfully!',
  'Change password successfully' => 'Change password successfully!',
  'Your password not correct,please try again' => 'Your password not correct,please try again!',
  'Confirm new password not same' => 'Confirm new password not same',
  'There are errors please contact the admin website' => 'There are errors please contact the admin website!',
  'Please check info after booking' => 'Please check info after booking!',
  'Your booking has been send to Admin, please wait confirm' => 'Your booking has been send to Admin, please wait confirm!',
  'This room has been booked, please change room or change day stay' => 'This room has been booked, please change room or change day stay!',
  'History booking' => 'History booking',
  'Waitting Confirm' => 'Waitting Confirm',
  'Confirmed' => 'Confirmed',
  'Paid' => 'Paid',
  'Invalid Email or Password' => 'Invalid Email or Password!',
  'Your account looked, please contact admin' => 'Your account looked, please contact admin!',
  'Login Successfully' => 'Login Successfully!',
  'Your email already exist in system, please try again' => 'Your email already exist in system, please try again!',
  'Confirm password not same' => 'Confirm password not same!',
  'Register Account Successfully' => 'Register Account Successfully!',
  'Email not found please try again' => 'Email not found please try again!',
  'A request change password has been sent to your email, maybe the email is in the spam box, please check' => 'A request change password has been sent to your email, maybe the email is in the spam box, please check!',
  'Please Sign In or Sign Up' => 'Please Sign In or Sign Up!',
  'Wrong your account, please contact admin' => 'Wrong your account, please contact admin!',
);
