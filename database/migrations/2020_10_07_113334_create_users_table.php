<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('email');
            $table->string('password');
            $table->string('name');
            $table->integer('gender')->nullable();
            $table->date('birth_day')->nullable();
            $table->string('avatar')->nullable();
            $table->string('address')->nullable();
            $table->string('identify_card')->nullable();
            $table->string('phone')->nullable();
            $table->string('remember_token')->nullable();   
            $table->integer('status');  
            $table->integer('created_by')->nullable();     
            $table->integer('updated_by')->nullable();
            $table->softDeletes();
            $table->timestamps();

            $table->unsignedBigInteger('role_id');
            $table->foreign('role_id')->references('id')->on('roles')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
        // Schema::table('users', function (Blueprint $table) {
        //     $table->dropSoftDeletes();
        // });
    }
}
