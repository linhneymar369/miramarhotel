<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Role;
use DB;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            [
                'id' => 1,
                'name'=> "Admin",
                'description'=> 'Quan ly tat ca he thong, nhan vien, user, khach hang.',
                'created_at' => date('Y-m-d H:i:s'),
                'created_by' => 1,
                'updated_at' => date('Y-m-d H:i:s'),
                'updated_by' => 2,
            ],
            [
                'id' => 2,
                'name'=> "Employee",
                'description'=> 'Quan ly tat ca he thong, user, khach hang.',
                'created_at' => date('Y-m-d H:i:s'),
                'created_by' => 1,
                'updated_at' => date('Y-m-d H:i:s'),
                'updated_by' => 2,
            ],
            [
                'id' => 3,
                'name'=> "Customer",
                'description'=> 'Khach hang',
                'created_at' => date('Y-m-d H:i:s'),
                'created_by' => 1,
                'updated_at' => date('Y-m-d H:i:s'),
                'updated_by' => 1,
            ],
        ]);
    }
}
