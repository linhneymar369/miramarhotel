<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class ServiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('services')->insert([
            [
                'id' => 1,
                'name'=> "Nothing",
                'price'=> '0',
                'created_at' => date('Y-m-d H:i:s'),
                'created_by' => 1,
                'updated_at' => date('Y-m-d H:i:s'),
                'updated_by' => 1,
            ],
            [
                'id' => 2,
                'name'=> "Outdoor Pool",
                'price'=> '5',
                'created_at' => date('Y-m-d H:i:s'),
                'created_by' => 1,
                'updated_at' => date('Y-m-d H:i:s'),
                'updated_by' => 1,
            ],
            [
                'id' => 3,
                'name'=> "Spa Zone",
                'price'=> '20',
                'created_at' => date('Y-m-d H:i:s'),
                'created_by' => 1,
                'updated_at' => date('Y-m-d H:i:s'),
                'updated_by' => 1,
            ],
            [
                'id' => 4,
                'name'=> "Buffet Breakfast",
                'price'=> '5',
                'created_at' => date('Y-m-d H:i:s'),
                'created_by' => 1,
                'updated_at' => date('Y-m-d H:i:s'),
                'updated_by' => 1,
            ],
            [
                'id' => 5,
                'name'=> "Sky Bar",
                'price'=> '50',
                'created_at' => date('Y-m-d H:i:s'),
                'created_by' => 1,
                'updated_at' => date('Y-m-d H:i:s'),
                'updated_by' => 1,
            ],
        ]);
    }
}
