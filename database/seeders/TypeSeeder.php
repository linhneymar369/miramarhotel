<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Type;
use Carbon\Carbon;
class TypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Type::insert([
            [
                'id' => 1,
                'name'=> "Standard Room",
                'price'=> 20,         
                'created_by' => 1,
                'updated_by' => 1,
            ],
            [
                'id' => 2,
                'name'=> "Intermediate Room",
                'price'=>  30,           
                'created_by' => 2,           
                'updated_by' => 2,
            ],
            [
                'id' => 3,
                'name'=> "Luxury Room",
                'price'=>  50,              
                'created_by' => 1,
                'updated_by' => 1,
            ],
            [
                'id' => 4,
                'name'=> "VIP Room",
                'price'=>  100,                
                'created_by' => 1,
                'updated_by' => 1,
            ],
            [
                'id' => 5,
                'name'=> "Presidential Room",
                'price'=>  500,             
                'created_by' => 1,
                'updated_by' => 1,
            ],
        ]);
    }
}
