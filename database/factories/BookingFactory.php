<?php

namespace Database\Factories;

use App\Models\Booking;
use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\{Room,User,Service};
class BookingFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Booking::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $rooms = Room::all();
        $services = Service::all();
        $users = User::all();
        return [
            'room_id'=> $rooms->random(),
            'service_id'=> $services->random()->id,
            'user_id'=> $users->random()->id,
            'check_in' => $this->faker->dateTime($max = 'now', $timezone = null),
            'check_out' => $this->faker->dateTime($max = 'now', $timezone = null),
            //faker total đúng với kiểu (check_out - check-in)*rooms->types->price + service
            'total' => $this->faker->numberBetween($min = 200, $max = 3000),
            'status' => $this->faker->randomElement($array = array (1,2,3,4)),
            'created_by' => $this->faker->randomElement($array = array (1,2)),
            'updated_by' => $this->faker->randomElement($array = array (1,2)),
        ];
    }
}
