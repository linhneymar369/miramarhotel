<?php

namespace Database\Factories;

use App\Models\Role;
use Illuminate\Database\Eloquent\Factories\Factory;
use Carbon;
class RoleFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Role::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->randomElement($array = array ('Employee','Admin','Customer')),
            'description' =>$this->faker->text,
            'created_by'=> $this->faker->randomElement($array = array (1,2)),
            'updated_by'=> $this->faker->randomElement($array = array (1,2)),
    
        ];
    }
}
