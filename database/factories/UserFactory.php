<?php

namespace Database\Factories;

use App\Models\{User,Role};
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
      
        $roles = Role::all();
            return [
            'name' => $this->faker->name,
            'email' => $this->faker->freeEmail,   
            'password' => $this->faker->password,
            'gender' =>$this->faker->randomElement($array= array(1,2)),
            'birth_day' => $this->faker->date($format = 'Y-m-d', $max = 'now'),
            'address' => $this->faker->address,
            'identify_card' => $this->faker->creditCardNumber,
            'phone' => $this->faker->e164PhoneNumber,
            'status' => $this->faker->randomElement($array = array (0,1)),
            'role_id' => $roles->random(),          
            ];
       
    }
}
