<?php

namespace Database\Factories;

use App\Models\Model;
use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Rate;
use App\Models\User;
use App\Models\Room;
class RateFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Rate::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $users = User::all();
        $rooms = Room::all();
        return [
            'content' =>$this->faker->text,
            'user_id'=> $users->random(),
            'room_id'=> $rooms->random(),
            'created_by' => $this->faker->randomElement($array = array (1,2)),
            'updated_by' => $this->faker->randomElement($array = array (1,2)),
        ];
    }
}
