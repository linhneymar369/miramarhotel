<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Models\User;
use App\Models\Role;
use App\Models\Rate;
use App\Models\Booking;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = User::join('roles', 'users.role_id', '=','roles.id');
        $users = $query
                        ->select('users.*','roles.name as role_name')
                        ->whereIn('role_id',[1,3])
                        ->paginate(10);
        if($request->id){
            $id = $request->id;
            $users = $query
                        ->where('users.id',$id)
                        ->paginate(10);
        }
        if($request->name){
            $name =$request->name;
            $users = $query
                        ->where('users.name','LIKE', "%" . $name . "%")
                        ->paginate(10);
        }
        if($request->role){
            $role = $request->role;
            $users = $query
                    ->where('roles.name', 'LIKE', "%" . $role . "%")
                    ->paginate(10);
        }
        return view('admin.users.user', compact('users'));
    }
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::all();
        return view('admin.users.createuser',['users'=>$roles]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'email'=>'required|max:100|regex:/^.+@.+$/i|email|unique:users',
            'password'=>'required|max:50|',
            'name' => 'required|max:200',
            'birth_day' => 'required|date',
            'address' => 'required|max:255',
            'identify_card' => 'required|max:15',
            'phone' => 'required|max:15',
            'status' => 'required',
            'role_id' => 'required'
        ]);
        $user = new User;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->name = $request->name;
        $user->gender = $request->gender;
        $user->birth_day = $request->birth_day;
        $user->address = $request->address;
        $user->identify_card = $request->identify_card;
        $user->phone = $request->phone;
        $user->status = $request->status;
        $user->role_id = $request->role_id;
        $user->created_by = Auth::id();
        $user->save();
        return redirect('/users')->with('message', 'User created!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        $user = User::where('id',$id)->first();
        return view('admin.users.edituser',compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'email'=>'required|max:100|regex:/^.+@.+$/i',
            'name' => 'required|max:200',
            'status' => 'required',
            'role_id' => 'required'
        ]);
        $user = User::where('id',$id)->first();
        $user->email = $request->email;
        $user->name = $request->name;
        $user->status = $request->status;
        $user->role_id = $request->role_id;
        $user->update();
        return redirect('/users')->with('message', 'User updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $rates = Rate::where('user_id',$id)->get();
        foreach($rates as $rate) {
            $rate->delete();
        }
        $bookings = Booking::where('user_id',$id)->get();
        foreach($bookings as $booking) {
            $booking->delete();
        }
        $user = User::where('id',$id)->first();
        $user->delete();
        return redirect('/users')->with('message', 'User deleted!');
    }
}