<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Type;
use App\Models\Room;
use App\Models\Booking;
use Illuminate\Support\Facades\Auth;
class TypeRoomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $types = Type::all();
        return view('admin.type_room.typeroom',compact('types'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.type_room.createtype');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'=>'required|max:100',
            'price'=>'required|numeric',
        ]);
        $typeroom =  Type::create([
            'name' => $request->name,
            'price' =>$request->price,
            'created_by' =>Auth::id(),
            'updated_by' =>Auth::id()
        ]);
        $typeroom->save();
        return redirect('/types')->with('message', 'Type Room Created!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $type = Type::where('id',$id)->first();
        return view('admin.type_room.edittype',compact('type'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'=>'required|max:100',
            'price'=>'required|numeric',
        ]);
        $type = Type::where('id',$id)->first();
        $type->name = $request->name;
        $type->price = $request->price;
        $type->updated_by = Auth::id();
        $type->update();
        return redirect('/types')->with('message', 'Type updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        $type = Type::find($id);
        $type->rooms->each(function ($item, $key) {
            $item->bookings->each(function ($item, $key) {
                $item->delete();
            });
            $item->delete();
        });
        $type->delete();
        return redirect('/types')->with('message', 'Type deleted!');
    }
}
