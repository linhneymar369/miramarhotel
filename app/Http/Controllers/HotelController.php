<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use App\Models\{Room, Type, Booking, Service, Rate, User};

use DB;
class HotelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function home()
    {
        return view('customer.home');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function aboutus()
    {
        return view('customer.about');
    }
    public function detailroom($id){
        $detailroom = Room::where('id',$id)->first();
        $list_service = Service::where('id','!=',1)->get();
        $rates = Rate::where('room_id',$id)
                            ->orderBy('created_at','desc')
                            ->get();
        $count = count($rates);
        return view('customer.detailroom',compact('detailroom','list_service','rates','count'));
    }
    public function postRate(Request $request, $id){
        $request->validate([
            'content'=>'required|max:255',
        ]);
        $rate = new Rate;
        $rate->user_id =  Auth::id();
        $rate->room_id = $id;
        $rate->content = $request->content;
        $rate->created_by = Auth::id();
        $rate->updated_by = Auth::id();
        $rate->save();
        Session()->flash('message', 'Rate successfully!');
        return back();
    }
    public function createrate(Request $request)
    {
        
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function room(Request $request)
    {
        $query = Type::join('rooms', 'types.id', '=', 'rooms.type_id');
        $list_room = $query
            ->select('types.*', 'rooms.*')
            ->where('status',1)
            ->paginate(9);

        if($request->amount_user)
        {
            $amount_user = $request->amount_user;
            $list_room = $query
                        ->where('amount_user', $amount_user)
                        ->paginate(9);
        }
        if($request->type){
            $type = $request->type;
            $list_room = $query
                        ->where('type_id', $type)
                        ->paginate(9);
        }
        return view('customer.room',compact('list_room'));
    }
    public function searchroom(Request $request){
    }
    public function store(){
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function contact()
    // {
    //     return view('customer.contact');
    // }
    public function search()
    {
        return view('customer.room');
    }
    public function cart()
    {
        return view('customer.cart');
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}