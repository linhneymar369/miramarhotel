<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Service;
use App\Models\Booking;
use Illuminate\Support\Facades\Auth;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list_service = Service::where('id','!=',1)->get();
        return view('admin.servicess.service',compact('list_service'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $services = Service::all();
        return view ('admin.servicess.createservice',['services'=>$services]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'=>'required|max:125',
            'price'=>'required|numeric',
        ]);
        $service =  Service::create([
            'name' => $request->name,
            'price' =>$request->price,
            'created_by' =>Auth::id(),
            'updated_by' =>Auth::id()
        ]);
        $service->save();
        return redirect('/services')->with('message', 'Service created!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $service = Service::where('id',$id)->first();
        return view('admin.servicess.editservice',compact('service'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'=>'required|max:125',
            'price'=>'required|numeric',
        ]);
        $service = Service::where('id',$id)->first();
        $service->name = $request->name;
        $service->price = $request->price;
        $service->updated_by = Auth::id();
        $service->update();
        return redirect('/services')->with('message', 'Service updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $bookings = Booking::where('service_id',$id)->get();
        foreach($bookings as $booking) {
            $booking->delete();
        }
        $service = Service::where('id',$id)->first();
        $service->delete();
        return redirect('/services')->with('message', 'Service deleted!');
    }
}
