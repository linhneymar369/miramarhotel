<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;

class RegisterController extends Controller
{
    public function sendRegister(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required',
            'password' => 'required|min:10|max:255',
            're_password' => 'required|min:10|max:255'
        ]);

        $checkEmail = User::where('email', $request->email)->get()->first();
        if ($checkEmail !== null) {
            Session()->flash('message', __('sentence.Your email already exist in system, please try again'));
            return back();
        } else {
            if ($request->password !== $request->re_password) {
                Session()->flash('message', __('sentence.Confirm password not same'));
                return back();
            } else {
                $user = new User;
                $user->name = $request->name;
                $user->email = $request->email;
                $user->password = bcrypt($request->re_password);
                $user->status = 1;
                $user->role_id = 3;
                $user->save();
                Session()->flash('message', __('sentence.Register Account Successfully'));
                return back();
            }
        }
    }
}