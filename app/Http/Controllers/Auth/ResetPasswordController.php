<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use App\Models\PasswordReset;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Mail;

class ResetPasswordController extends Controller
{
    public function sendEmailPassword(Request $request)
    {
        $request->validate([
            'email' => 'required'
        ]);

        $email = $request->email;


        $resultEmail = User::where('email', $email)->first();
        if (empty($resultEmail)) {
            Session()->flash('message', __('sentence.Email not found please try again'));
            return back();
        } else {
            $resultName = User::where('email', $email)->first();
            $resultReset = new PasswordReset;
            $resultReset->email = $email;
            $resultReset->token = Str::random(80);
            $resultReset->expiration_date = now()->addMinutes(5);
            $resultReset->save();
            $this->email = $resultReset['email'];
            $this->token = $resultReset['token'];
            $this->name = $resultName['name'];
            Mail::send('emails.mailreset', array('name' => $this->name, 'token' => $this->token), function ($message) {
                $message->to($this->email, $this->name)->subject('Reset password');
            });
            Session()->flash('message', __('sentence.A request change password has been sent to your email, maybe the email is in the spam box, please check'));
            return back();
        }
    }
}