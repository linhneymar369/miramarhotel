<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;

class LoginController extends Controller
{
    public function sendLogin(Request $request)
    {
        $check = $request->validate([
            'email' => 'required|email',
            'password' => 'required|max:255'
        ]);

        $remember = ($request->remember_me) ? true : false;

        if (Auth::attempt($check, $remember)) {
            $user = Auth::user();
        
            if ($user->role_id != 3) {
                $this->doLogout();
                Session()->flash('message', __('sentence.Invalid Email or Password'));
                return back();
            } elseif($user->status == 1){
                Session()->flash('message', __('sentence.Login Successfully'));
                return redirect('/user_profile');
            }else{
                $this->doLogout();
                Session()->flash('message',__('sentence.Your account looked, please contact admin'));
                return back();
            }
        } else {
            $request->session()->flash('message', __('sentence.Invalid Email or Password'));
            return back();
        }

    }

    public function doLogout()
    {
        Auth::logout();
        return redirect('/');
    }
    public function doLogoutAdmin()
    {
        Auth::logout();
        return redirect('/admin');
    }

    public function doLoginAdmin(Request $request){
        $check = $request->validate([
            'email' => 'required|email',
            'password' => 'required'
        ]);

        $remember = ($request->remember_me) ? true : false;

        if (Auth::attempt($check, $remember)) {
            $user = Auth::user();
            if ($user->role_id ==3) {
                $this->doLogout();
                Session()->flash('message', 'Invalid Email or Password!');
                return back();
            } else {
                return redirect('/admin');
            }
        } else {
            $request->session()->flash('message', 'Invalid Email or Password!');
            return back();
        }
    }
}