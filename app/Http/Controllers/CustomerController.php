<?php

namespace App\Http\Controllers;

use App\Models\{User, Booking, PasswordReset, Room, Service};
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use DB;


class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::find(Auth::id());
        return view('usercustomer.profile', ['data' => $user]);
    }
    public function doUserEdit(Request $request)
    {
        $request->validate([
            'name' => 'required|min:8|max:255',
            'address' => 'required|max:255',
            'phone' => 'required|max:15',
            'birth_day' => 'required',
            'identify_card' => 'required|max:15',
            'gender' => 'required|numeric'
        ]);
        $user = User::where('id', Auth::id())->get()->first();
        $user->name = $request->name;
        $user->address = $request->address;
        $user->phone = $request->phone;
        $user->birth_day = $request->birth_day;
        $user->identify_card = $request->identify_card;
        $user->gender = $request->gender;
        $user->updated_by = Auth::id();
        $user->save();
        Session()->flash('message', __('sentence.Edit your profile successfully'));
        return redirect('/user_profile');
    }
    public function changeAvatar(Request $request)
    {
        $request->validate([
            'avatar' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $image = $request->file('avatar');
        $user = User::where('id', Auth::id())->get()->first();
        $name  = $user->name;
        $nowImageName = Str::slug($name) . '-' . time() . '.' . $image->getClientOriginalExtension();
        if ($user->avatar != null) {
            $filename = public_path("images/avatar/$user->avatar");
            File::delete($filename);
        }
        $image->move(public_path('images/avatar'), $nowImageName);
        $user->avatar = $nowImageName;
        $user->save();
        Session()->flash('message', __('sentence.Change avatar successfully'));
        return redirect('/user_profile');
    }
    public function showPasswordChange()
    {
        return view('usercustomer.resetpassword');
    }

    public function doChangePassword(Request $request)
    {
        $request->validate([
            'oldPassword' => 'required|min:10|max:255',
            'newPassword' => 'required|min:10|max:255',
            'confirmPassword' => 'required|min:10|max:255'
        ]);

        $oldPassword = $request->oldPassword;
        $newPassword = $request->newPassword;
        $confirmPassword = $request->confirmPassword;
        if ($newPassword === $confirmPassword) {
            $user = User::where('id', Auth::id())->get()->first();
            $userPassword = $user->password;
            if (Hash::check($oldPassword, $userPassword) === true) {
                $user->password = Hash::make($confirmPassword);
                $user->updated_by = Auth::id();
                $user->save();
                Session()->flash('message', __('sentence.Change password successfully'));
                return redirect('/user_profile');
            } else {
                Session()->flash('message', __('sentence.Your password not correct,please try again'));
                return back();
            }
        } else {
            Session()->flash('message', __('sentence.Confirm new password not same'));
            return back();
        }
    }
    public function showHistory()
    {
        $user = User::where('id', Auth::id())->first();
        $waits = Booking::where('status', '=', 2)
            ->where('user_id', '=', Auth::id())
            ->get();
        $confirms = Booking::where('status', '=', 3)
            ->where('user_id', '=', Auth::id())
            ->get();
        $paids = Booking::where('status', '=', 4)
            ->where('user_id', '=', Auth::id())
            ->get();
        return view('customer.history', compact(['user', 'waits', 'confirms', 'paids']));
    }

    public function getChangePass($token)
    {
        $result = PasswordReset::where('token', $token)->first();
        if (!empty($result)) {
            $resultDate = $result->expiration_date;
            if (now() <= $resultDate) {
                $token = $result['token'];
                return view('customer.resetpass')->with('token', $token);
            } else {
                PasswordReset::where('token', $token)->delete();
                return view('error.403');
            }
        } else {
            return view('error.403');
        }
    }
    public function postChangePass(Request $request)
    {
        $request->validate([
            'password' => 'required', 'confirmed',
            're_password' => 'required'
        ]);

        $rePassword = $request['re_password'];
        $token = $request['token'];
        $checkToken = PasswordReset::where('token', $token)->get()->first();

        if ($checkToken) {
            $findEmail = PasswordReset::where('token', $token)->get()->first();
            $updatePassword = User::where('email', $findEmail->email)->first();
            $updatePassword->password = Hash::make($rePassword);
            $updatePassword->save();
            if ($updatePassword) {
                PasswordReset::where('token', $token)->delete();
                Session()->flash('message_success', __('sentence.Change password successfully'));
                return view('customer.success_change_password');
            } else {
                Session()->flash('message_error', __('sentence.There are errors please contact the admin website'));
                return back();
            }
        } else {
            return view('error.403');
        }
    }
    public function pushToCart(Request $request, $id)
    {
        $cart = new Booking;
        $cart->room_id = $id;
        $cart->user_id = Auth::id();
        $cart->service_id = 5;
        $cart->check_in = Carbon::now();
        $cart->check_out = Carbon::tomorrow();
        $cart->status = 1;
        $cart->total = Room::where('id', $id)->first()->types->price;
        $cart->created_by = Auth::id();
        $cart->updated_by = Auth::id();
        $cart->save();
        return redirect('/cart')->with('message', __('sentence.Please check info after booking'));
    }
    public function sendBookingToAdmin(Request $request, $id)
    {
        $request->validate([
            'check_in' => 'required|after:now',
            'check_out' => 'required|after:now',
            'total' => 'required'
        ]);
        $booking = Booking::where('id', $id)->first();
        if (
            DB::table('bookings')
            ->where('room_id', $booking->room_id)
            ->where('status', '!=', 1)
            ->where(function ($book) use ($request) {
                $book->where(function ($query) use ($request) {
                    $query->whereBetween('check_in', [$request->check_in, $request->check_out])
                        ->orWhereBetween('check_out', [$request->check_in, $request->check_out]);
                })
                    ->orWhere(function ($check) use ($request) {
                        $check->whereRaw("'$request->check_in' BETWEEN check_in AND check_out")
                            ->orWhereRaw("'$request->check_out' BETWEEN check_in AND check_out");
                    });
            })
            ->get()->isEmpty()
        ) {
            $booking->service_id = $request->service;
            $booking->check_in = $request->check_in;
            $booking->check_out = $request->check_out;
            $booking->total = $request->total;
            $booking->status = 2;
            $booking->updated_by = Auth::id();
            $booking->update();
            return redirect('/cart')->with('message', __('sentence.Your booking has been send to Admin, please wait confirm'));
        } else {
            return redirect('/cart')->with('message', __('sentence.This room has been booked, please change room or change day stay'));
        }
    }
    public function showCart()
    {
        $carts = Booking::where('status', '=', 1)
            ->where('user_id', '=', Auth::id())
            ->get();
        $services = Service::all();
        return view('customer.cart', compact(['carts', 'services']));
    }
    public function cartAjax(Request $request, $id)
    {
        $id = $request->id;
        $service = Service::where('id', $id)->first();
        return response()->json(array('success' => true, 'price' => $service->price));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $booking = Booking::where('id', $id)->first();
        $booking->delete();
        return redirect('/cart')->with('message', 'Booking deleted!');
    }
}
