<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\User;
use App\Models\Role;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\File;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        return view('admin.index');
    }
    public function profile()
    {
        $user = User::where('id',Auth::id())->first();
        return view('admin.profile', ['data' => $user]);
    }
    public function editprofile(Request $request){
        $user = User::where('id', Auth::id())->get()->first();
        $user->name = $request->name;
        $user->gender = $request->gender;
        $user->identify_card = $request->identify_card;
        $user->birth_day = $request->birth_day;
        $user->address = $request->address;
        $user->phone = $request->phone;
        $user->updated_by = Auth::id();
        $user->save();
        Session()->flash('message','Updated info successfully!');
        return redirect('/admin/profile');

    }
    public function doChangePassword(Request $request)
    {
        $request->validate([
            'oldPassword' => 'required|min:10|max:255',
            'newPassword' => 'required|min:10|max:255',
            'confirmPassword' => 'required|min:10|max:255'
        ]);

        $oldPassword = $request->oldPassword;
        $newPassword = $request->newPassword;
        $confirmPassword = $request->confirmPassword;
        if ($newPassword === $confirmPassword) {
            $user = User::where('id', Auth::id())->get()->first();
            $userPassword = $user->password;
            if (Hash::check($oldPassword, $userPassword) === true) {
                $user->password = Hash::make($confirmPassword);
                $user->updated_by = Auth::id();
                $user->save();
                Session()->flash('message', 'Change new password successfully!');
                return redirect('/admin/profile');
            } else {
                Session()->flash('message', 'Your password not correct,please try again!');
                return redirect('/admin/profile');
            }
        } else {
            Session()->flash('message', 'Confirm new password not same!');
            return redirect('/admin/profile');
        }
    }
    public function changeAvatar(Request $request){
        $request->validate([
            'avatar' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $image = $request->file('avatar');
        $user = User::where('id', Auth::id())->get()->first();
        $name  = $user->name;
        $nowImageName = Str::slug($name) . '-' . time() . '.' . $image->getClientOriginalExtension();
        if ($user->avatar != null) {
            $filename = public_path("images/avatar/$user->avatar");
            File::delete($filename);
        }
        $image->move(public_path('images/avatar'), $nowImageName);
        $user->avatar = $nowImageName;
        $user->save();
        Session()->flash('message', 'Change avatar successfully!');
        return back();
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
