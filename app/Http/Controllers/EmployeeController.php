<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use App\Models\Role;
use App\Models\Rate;
use App\Models\Booking;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {   
        $query = User::join('roles', 'users.role_id', '=','roles.id');
        $employees = $query->where('role_id','2')
                            ->select('users.*','roles.name as role_name')
                            ->paginate(10);
        if($request->id){
            $id = $request->id;
            $employees = $query
                        ->where('users.id',$id)
                        ->paginate(10);
        }
        if($request->name){
            $name =$request->name;
            $employees = $query
                        ->where('users.name','LIKE', "%" . $name . "%")
                        ->paginate(10);
        }
        // $employees = DB::table('roles')
        // ->join('users','roles.id','=', 'users.role_id')
        // ->where('role_id', '2')
        // ->select('users.*', 'roles.name as role_name')
        // ->get();
        return view('admin.employees.employee', compact('employees'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::all();
        return view('admin.employees.createemployee',['users'=>$roles]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'email'=>'required|max:100|regex:/^.+@.+$/i|email|unique:users',
            'password'=>'required|max:50|',
            'name' => 'required|max:200',
            'birth_day' => 'required|date',
            'address' => 'required|max:255',
            'identify_card' => 'required|numeric',
            'phone' => 'required|numeric',
            'status' => 'required',
            'role_id' => 'required'
        ]);
        $employee = User::create([
            'email' => $request->email,
            'password' =>$request->password,
            'name' =>$request->name,
            'gender' => $request->gender,
            'birth_day'=> $request->birth_day,
            'address' => $request->address,
            'identify_card' => $request->identify_card,
            'phone' => $request->phone,
            'status' => $request->status,
            'created_by' => Auth::id(),
            'updated_by' => Auth::id(),
            'created_at' => now(),
            'role_id' => 2,
        ]);
        $employee->save();
        return redirect('/employees')->with('message','Employee created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $employee)
    {
        return view('admin.employees.editemployee',compact('employee'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,User $employee)
    {
        $request->validate([
            'email'=>'required|max:100|regex:/^.+@.+$/i|email',
            'name' => 'required|max:200',
            'birth_day' => 'required|date',
            'address' => 'required|max:255',
            'identify_card' => 'required|max:15',
            'phone' => 'required|max:15',
            'status' => 'required',
        ]);
        $employee->email = $request->email;
        $employee->name = $request->name;
        $employee->gender = $request->gender;
        $employee->birth_day = $request->birth_day;
        $employee->address = $request->address;
        $employee->identify_card = $request->identify_card;
        $employee->phone = $request->phone;
        $employee->updated_by = Auth::id();
        $employee->updated_at = now(); 
        $employee->update();
        return redirect('/employees')->with('message', 'Employee updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $employee)
    {
        $rates = Rate::where('user_id',$id)->get();
        foreach($rates as $rate) {
            $rate->delete();
        }
        $bookings = Booking::where('user_id',$id)->get();
        foreach($bookings as $booking) {
            $booking->delete();
        }
        $employee->delete();
        return redirect('/employees')->with('message', 'Employee deleted!');
    }
}
