<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Models\Room;
use App\Models\Type;
use App\Models\Booking;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
class RoomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = Room::join('types','rooms.type_id','=','types.id');
        $rooms = $query
                    ->select('rooms.*','types.name as type_name','types.price as type_price')
                    ->paginate(10);
        if($request->id){
            $id = $request->id;
            $rooms= $query
                        ->where('rooms.id',$id)
                        ->paginate(10);
        }
        if($request->type){
            $type = $request->type;
            $rooms= $query
                        ->where('types.name','LIKE', "%" . $type . "%")
                        ->paginate(10);
        }
        if($request->status){
            $status = $request->status;
            $rooms = $query
                    ->where('rooms.status',$status)
                    ->paginate(10);
        }
        return view('admin.rooms.room', compact('rooms'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $rooms = Room::all();
        return view('admin.rooms.createroom',['rooms'=>$rooms]);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'img1'=>'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'img2'=>'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'img3' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'type_id' => 'required|numeric',
            'description' => 'required|max:1000',
            'amount_user' => 'required|numeric',
            'status' => 'required|numeric',
        ]);
        $file_name1 = '';
        $file_name2 = '';
        $file_name3 = '';
        if ($request->file('img1')) {
            $destinationPath = public_path('images/rooms');
            $files = $request->file('img1');
            $file_name = $files->getClientOriginalName(); 
            $file_name1 = $file_name;
            $files->move($destinationPath , $file_name); 
        } 
        if ($request->file('img2')) {
            $destinationPath = public_path('images/rooms');
            $files = $request->file('img2');
            $file_name = $files->getClientOriginalName();
            $file_name2 = $file_name; 
            $files->move($destinationPath , $file_name); 
        } 
        if ($request->file('img3')) {
            $destinationPath = public_path('images/rooms');
            $files = $request->file('img3');
            $file_name = $files->getClientOriginalName(); 
            $file_name3 = $file_name;
            $files->move($destinationPath , $file_name); 
        }
        $room =  Room::create([
            'img1' =>  $file_name1,
            'img2' => $file_name2,
            'img3' => $file_name3,
            'type_id'=> $request->type_id,
            'description'=> $request->description,
            'amount_user' => $request->amount_user,
            'status' => $request->status,
            'created_by' =>Auth::id(),
            'updated_by' => Auth::id(),
        ]);
        $room->save(); 
        return redirect('/rooms')->with('message','Room created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $room = Room::where('id',$id)->first();
        return view('admin.rooms.editroom',compact('room'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'type_id' => 'required|numeric',
            'description' => 'required|max:1000',
            'amount_user' => 'required|numeric',
            'status' => 'required|numeric',
        ]);
        $file_name1 = Room::where('id',$id)->first()->img1;
        $file_name2 = Room::where('id',$id)->first()->img2;
        $file_name3 = Room::where('id',$id)->first()->img3;
        if ($request->file('img1')) {
            $destinationPath = public_path('images/rooms');
            $files = $request->file('img1');
            $file_name = $files->getClientOriginalName(); 
            $file_name1 = $file_name;
            $files->move($destinationPath , $file_name); 
        } 
        if ($request->file('img2')) {
            $destinationPath = public_path('images/rooms');
            $files = $request->file('img2');
            $file_name = $files->getClientOriginalName();
            $file_name2 = $file_name; 
            $files->move($destinationPath , $file_name); 
        } 
        if ($request->file('img3')) {
            $destinationPath = public_path('images/rooms');
            $files = $request->file('img3');
            $file_name = $files->getClientOriginalName(); 
            $file_name3 = $file_name;
            $files->move($destinationPath , $file_name); 
        }
        $room = Room::where('id',$id)->first();
        $room->img1 = $file_name1;
        $room->img2 = $file_name2;
        $room->img3 = $file_name3;
        $room->type_id = $request->type_id;
        $room->description = $request->description;
        $room->amount_user = $request->amount_user;
        $room->status = $request->status;
        $room->updated_by = Auth::id();
        $room->updated_at = now(); 
        $room->update();
        return redirect('/rooms')->with('message', 'Room updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $bookings = Booking::where('room_id',$id)->get();
        foreach($bookings as $booking) {
            $booking->delete();
        }
        $room = Room::where('id',$id)->first();
        $room->delete();
        return redirect('/rooms')->with('message', 'Room deleted!');
    }
}
