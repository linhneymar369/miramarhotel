<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Auth;
use App\Models\{Booking,Service,User,Room};

class BookingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {   
        $query = Booking::query();
        if($request->status){
            $status = $request->status;
            $query = $query
                        ->where('bookings.status','=', $status );
        }
        if($request->username){
            $username = $request->username;
            $query = $query
                        ->join('users', 'users.id', '=', 'bookings.user_id')
                        ->select('bookings.*','users.name')
                        ->where('users.name', 'LIKE', "%" . $username . "%");
        }
        if($request->idroom){
            $idroom = $request->idroom;
            $query = $query
                ->join('rooms', 'rooms.id', '=', 'bookings.room_id')
                ->where('rooms.id', '=', $idroom);
        }
        if($request->typeroom){
            $type = $request->typeroom;
            $query = $query
                    ->join('rooms', 'rooms.id', '=', 'bookings.room_id')
                    ->where('type_id','=',$type);
        }
        // dd($query->get());
        $bookings = $query->with('rooms','services','users')->orderByDesc('bookings.created_at')->paginate(20);
        // dd($bookings);
        return view('admin.bookings.booking',compact('bookings'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $services = Service::all();
        $users = User::all();
        return view('admin.bookings.createbooking',compact(['services','users']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'room'=>'required|numeric',
            'service'=>'required|numeric',
            'user' => 'required|numeric',
            'checkin' => 'required|date',
            'checkout' => 'required|date',
            'total' => 'required|numeric',
            'status' => 'required|numeric',
        ]);
        $booking = new Booking;
        $booking->room_id = $request->room;
        $booking->service_id = $request->service;
        $booking->user_id = $request->user;
        $booking->check_in = $request->checkin;
        $booking->check_out = $request->checkout;
        $booking->total = $request->total;
        $booking->status = $request->status;
        $booking->created_by = Auth::id();
        $booking->updated_by = Auth::id();
        $booking->save();
        return redirect('/bookings')->with('message','Booking created!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $booking = Booking::find($id);
        $services = Service::all();
        $users = User::all();
        return view('admin.bookings.editbooking',compact(['booking','services','users']));
    }
     /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function printbill($id)
    {
        $booking = Booking::find($id);
        return view('admin.bookings.bill',compact("booking"));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'room'=>'required|numeric',
            'service'=>'required|numeric',
            'user' => 'required|numeric',
            'checkin' => 'required|date',
            'checkout' => 'required|date',
            'total' => 'required|numeric',
            'status' => 'required|numeric',
        ]);
        $booking = Booking::find($id);
        $booking->room_id = $request->room;
        $booking->service_id = $request->service;
        $booking->user_id = $request->user;
        $booking->check_in = $request->checkin;
        $booking->check_out = $request->checkout;
        $booking->total = $request->total;
        $booking->status = $request->status;
        $booking->updated_by = Auth::id();
        $booking->update();
        return redirect('/bookings')->with('message','Booking updated!');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $booking = Booking::find($id);
        $booking->delete();
        return redirect('/bookings')->with('message','Booking deleted!');
    }
}