<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class User
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (Auth::user()) {
            if (Auth::user()->role_id === 3 && Auth::check() === true) {
                return $next($request);
            } else {
                return redirect('/')->with('message', __('sentence.Wrong your account, please contact admin'));
                
            }
        } else {
            return redirect('/')->with('message',__('sentence.Please Sign In or Sign Up'));
        }
    }
}