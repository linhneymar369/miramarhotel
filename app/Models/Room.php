<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Type;
use App\Models\Booking;

class Room extends Model
{
    use SoftDeletes;
    use HasFactory;
    protected $table = 'rooms';
    protected $guarded = [];
    protected $hidden = [
        'deleted_at'
        ];
    public function types(){
        return $this->belongsTo(Type::class,'type_id');
    }
    public function bookings(){
        return $this->hasMany(Booking::class);
    }
}
