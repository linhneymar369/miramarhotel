<?php

namespace App\Models;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Room;

class Type extends Model
{
    use SoftDeletes;
    use HasFactory;
    protected $table = 'types';
    protected $guarded = [];
    protected $hidden = [
        'deleted_at'
        ];
    public function rooms(){
        return $this->hasMany(Room::class);
    }
}
