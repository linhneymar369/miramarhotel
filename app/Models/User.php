<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\{Role, Booking};
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;

class User extends Model implements AuthenticatableContract
{
    use SoftDeletes;
    use HasFactory;
    use Authenticatable;
    
    protected $table = 'users';
    protected $guarded = [];
    protected $fillable = [
        'name', 'email', 'password',
    ];
    protected $hidden = [
    'password', 'remember_token','deleted_at'
    ];
    public function roles(){
        return $this->belongsTo(Role::class);
    }

    public function bookings(){
        return $this->hasMany(Booking::class);
    }

    public function delete()
    {
        DB::transaction(function () {
            $this->bookings()->delete();
            return parent::delete();
        });
    } 
}