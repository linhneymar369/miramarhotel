<?php

namespace App\Models;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Room;
use App\Models\User;
class Rate extends Model
{
    use SoftDeletes;
    use HasFactory;
    protected $table = 'rates';
    protected $guarded = [];
    protected $hidden = [
        'deleted_at'
        ];

    public function rooms(){
        return $this->belongsTo(Room::class,'room_id');
    }
    public function users(){
        return $this->belongsTo(User::class,'user_id');
    }
}
