<?php

namespace App\Models;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Hotel extends Model
{
    use SoftDeletes;
    use HasFactory;
    protected $table = 'hotels';
    protected $guarded = [];
    protected $hidden = [
        'deleted_at'
        ];
}
