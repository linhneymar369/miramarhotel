<?php

namespace App\Models;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;
class Role extends Model
{
    use SoftDeletes;
    use HasFactory;
    protected $table = 'roles';
    protected $guarded = [];
    protected $hidden = [
        'deleted_at'
        ];
    public function users(){
        return $this->hasMany(User::class);
    }
}
